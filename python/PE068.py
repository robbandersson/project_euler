#
# Solution to Project Euler problem 68 - Magic 5-gon ring
#
# https://gitlab.com/robbandersson/project_euler
#
from itertools import permutations


def compute():
    num_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    max_val = '0'
    for perm in permutations(num_list, 9):

        line_1 = [10, perm[5], perm[6]]
        line_2 = [perm[0], perm[6], perm[7]]
        line_3 = [perm[1], perm[7], perm[8]]
        line_4 = [perm[2], perm[8], perm[4]]
        line_5 = [perm[3], perm[4], perm[5]]

        if sum(line_1) == sum(line_2) == sum(line_3) == sum(line_4) == sum(line_5):
            min_val = min(line_2[0], line_3[0], line_4[0], line_5[0])
            if min_val in line_2:
                concat_val = ''.join([str(x) for x in line_2 + line_3 + line_4 + line_5 + line_1])
            elif min_val in line_3:
                concat_val = ''.join([str(x) for x in line_3 + line_4 + line_5 + line_1 + line_2])
            elif min_val in line_4:
                concat_val = ''.join([str(x) for x in line_4 + line_5 + line_1 + line_2 + line_3])
            else:
                concat_val = ''.join([str(x) for x in line_5 + line_1 + line_2 + line_3 + line_4])

            if concat_val > max_val:
                max_val = concat_val

    return max_val

if __name__ == '__main__':
    print(compute())

