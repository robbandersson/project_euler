#
# Solution to Project Euler problem 95 - Amicable chains
#
# https://gitlab.com/robbandersson/project_euler
#
import time
import math
from src.functions import generate_primes


def get_proper_divisors(n):
    divisors = set()
    divisors.add(1)
    for x in range(2, int(math.sqrt(n + 1))):
        if n % x == 0:
            divisors.add(x)
            divisors.add(n // x)
    return divisors


def compute():
    start_time = time.time()
    n = 1
    upper = 1000000
    longest_chain = 0
    result = 0
    seen_num = set()
    while n < upper:
        if n in seen_num:
            n += 1
            continue
        else:
            chain = []
            m = n
            while m not in chain:
                seen_num.add(m)
                chain.append(m)
                m = sum(get_proper_divisors(m))
                if m > upper or m == 1:
                    break
            if m != 1 and sum(get_proper_divisors(m)) != m and m < upper:
                chain = chain[chain.index(m):]
                if len(chain) > longest_chain:
                    longest_chain = len(chain)
                    result = min(chain)
                    print(chain, min(chain))
            n += 1

    print('Elapsed time:', round(time.time() - start_time, 2), 'seconds.')
    return result, longest_chain


if __name__ == '__main__':
    print(compute())
