#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#

pentSum=[]
pentDiff=[]
pentList=[]
n=1
loopBreaker=False
while True:
    pentNbr = (n * (3 * n - 1) / 2)
    for i in range(len(pentList)-1,0,-1):
        if pentNbr-pentList[i] in pentList:
            pentDiff.append(pentNbr-pentList[i])
            pentSum.append(pentNbr+pentList[i])
    pentList.append(pentNbr)
    n+=1
    for j in range(len(pentSum)-1):
        if pentSum[j] in pentList:
            loopBreaker=True
            print(pentDiff[j])
    if loopBreaker:
        break


