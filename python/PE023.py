#
# Solution to Project Euler problem 23 - Non-abundant sums
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools as it
from itertools import accumulate
from operator import mul


def prod(lst):
    for value in accumulate(lst, mul):
        pass
    return value


def find_prim_factors(num):
    i = 2
    factors = []
    while num % i == 0:
        num = num / i
        factors.append(i)
    i = 3
    while i * i <= num:
        while num % i == 0:
            num = num / i
            factors.append(i)
        i += 2
    if num > 1:
        factors.append(int(num))
    return factors


def power_set(lst):
    pow_set = set()
    for i in range(1, len(lst) + 1):
        for sub_sets in it.combinations(lst, i):
            pow_set.add(sub_sets)
    return list(pow_set)


def find_proper_divisors(num):
    if num == 0:
        return [0]
    else:
        divisor_list = [1]
        prime_factors = find_prim_factors(num)
        prime_permute = power_set(prime_factors)
        for i in range(len(prime_permute)):
            divisor_list.append(prod(prime_permute[i]))
        divisor_list.remove(num)
        return divisor_list


def compute():
    tot_num = 28123
    n = 1
    abundant = []
    while n < tot_num:
        if sum(find_proper_divisors(n)) > n:
            abundant.append(n)
        n += 1

    # find all possible combinations of tuples in the list of abundant
    list_of_tuples = list(it.combinations_with_replacement(abundant, 2))

    # find all possible sum of two abundant numbers
    sum_of_abundant_list = []
    for i in range(0, len(list_of_tuples)):
        sum_of_abundant_list.append(sum(list_of_tuples[i]))

    # remove all duplicates
    sum_of_abundant_list = list(dict.fromkeys(sum_of_abundant_list))

    # find integers that cannot be written as a sum of two abundant numbers
    non_two_abundant = []
    for i in range(1, tot_num):
        if i not in sum_of_abundant_list:
            non_two_abundant.append(i)

    sum_of_non_two_abundant = sum(non_two_abundant)
    # print(non_two_abundant)
    return sum_of_non_two_abundant


if __name__ == '__main__':
    print(compute())
