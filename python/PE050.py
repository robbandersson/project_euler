#
# Solution to Project Euler problem 50 - Consecutive prime sum
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    target_prime = 0
    prime_list = [2]
    for n in range(3, 1000000, 2):
        if is_prime(n):
            prime_list.append(n)

    longest_consecutive = 0
    for i in range(len(prime_list)):
        prime_sum = prime_list[i]
        consecutive = 1
        for j in range(i + 1, len(prime_list)):
            prime_sum += prime_list[j]
            consecutive += 1
            if prime_sum > prime_list[-1]:
                break
            if prime_sum in prime_list and consecutive > longest_consecutive:
                longest_consecutive = consecutive
                target_prime = prime_sum

    return target_prime

if __name__ == '__main__':
    print(compute())
