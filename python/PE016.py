#
# Solution to Project Euler problem 16 - Power digit sum
#
# https://gitlab.com/robbandersson/project_euler
#
import math
from decimal import *


def compute():
    power_num = str(int(math.pow(Decimal(2), Decimal(1000))))
    tot = 0
    for i in range(len(power_num)):
        tot += int(power_num[i])

    return tot


if __name__ == '__main__':
    print(compute())
