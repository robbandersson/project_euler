#
# Solution to Project Euler problem 98 - Anagramic squares
#
# https://gitlab.com/robbandersson/project_euler
#
import os
file_path = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))


def compute():
    with open(data_dir + '\p098_words.txt') as f:
        text = f.read().replace("\"", "").split(",")

    # Start by filter away all words that are not anagrams
    sorted_words = list()
    for word in text:
        sorted_words.append(''.join(sorted(word)))
    idx = 0
    anagrams = list()
    for word in sorted_words:
        if sorted_words.count(word) > 1:
            anagrams.append(text[idx])
        idx += 1

    # Find the longest anagram
    max_len = 0
    for word in anagrams:
        if len(word) > max_len:
            max_len = len(word)

    # Generate all squares up to the longest anagram
    squares = set()
    num = 1
    square = num ** 2
    while square < 10 ** (max_len - 1):
        squares.add(square)
        num += 1
        square = num ** 2

    max_square = 0
    for word in anagrams:
        for square in squares:
            if len(str(square)) == len(word):
                letter_to_num = dict()
                idx = 0
                flag = True
                for letter in word:
                    if letter in letter_to_num:
                        if letter_to_num[letter] != str(square)[idx]:
                            flag = False
                            break
                    elif str(square)[idx] in letter_to_num.values():
                        flag = False
                        break
                    else:
                        letter_to_num[letter] = str(square)[idx]
                    idx += 1
                if flag:
                    # Find anagram
                    for second_word in anagrams:
                        if ''.join(sorted(word)) == ''.join(sorted(second_word)) and word != second_word:
                            num = ''
                            for letter in second_word:
                                num += str(letter_to_num[letter])
                            if num[0] != '0' and int(num) in squares:
                                if max(int(num), square) > max_square:
                                    max_square = max(int(num), square)

    return max_square


if __name__ == '__main__':
    print(compute())
