#
# Solution to Project Euler problem 41 - Pandigital prime
#
# https://gitlab.com/robbandersson/project_euler
#

from src.functions import is_prime, is_pandigital


def compute():
    highest_prime = 0
    for num in range(3, 10000000, 2):
        if is_pandigital(num):
            if is_prime(num):
                highest_prime = num

    return highest_prime


if __name__ == '__main__':
    print(compute())
