#
# Solution to Project Euler problem 89 - Roman numerals
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import re
file_path= os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))


def replace_pattern(numeral):
    search = ['DCCCC', 'LXXXX', 'VIIII', 'CCCC', 'XXXX', 'IIII']
    replacements = ['CM', 'XC', 'IX', 'CD', 'XL', 'IV']
    i = 0
    for pattern in search:
        if pattern in numeral:
            numeral = re.sub(pattern, replacements[i], numeral)
        i += 1
    return numeral\


def compute():
    with open(data_dir + '\p089_roman.txt') as f:
        input = f.read().splitlines()

    for i in range(15):
        print(input[i], replace_pattern(input[i]))
    num_saved = 0
    for entry in input:
        num_saved += len(entry) - len(replace_pattern(entry))
    return num_saved


if __name__ == '__main__':
    print(compute())
