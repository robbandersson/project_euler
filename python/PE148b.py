#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
def pascal_triangle(rows, divisor):
    last_row = [[1]]
    num = 1
    #results = []
    for count in range(1, rows):
        row = [1]
        for element in range(1, count):
            row.append((last_row[element - 1] + last_row[element]) % divisor)
        row.append(1)
        num += len(row) - row.count(0)
        last_row = row
        #results.append(row)
        #results.append(num)
    return num#, results


pascal = pascal_triangle(100000, 7)
print(pascal)