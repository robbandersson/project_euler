#
# Solution to Project Euler problem 1 - Multiples of 3 or 5
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    tot = 0
    for i in range(1, 1000):
        if i % 3 == 0 or i % 5 == 0:
            tot += i
    return tot


if __name__ == '__main__':
    print(compute())
