#
# Solution to Project Euler problem 96 - Su Doku
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import numpy as np
file_path = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))


def square_num(sudoku, row, col):
    sudoku_square = sudoku[row // 3 * 3:row // 3 * 3 + 3, col // 3 * 3:col // 3 * 3 + 3]
    return sudoku_square[np.nonzero(sudoku_square)]


def row_num(sudoku, row):
    sudoku_row = sudoku[row, :]
    return set(sudoku_row[np.nonzero(sudoku_row)])


def col_num(sudoku, col):
    sudoku_col = sudoku[:, col]
    return set(sudoku_col[np.nonzero(sudoku_col)])


def is_valid(sudoku, num, row, col):
    if num in square_num(sudoku, row, col) or num in row_num(sudoku, row) or num in col_num(sudoku, col):
        return False
    else:
        return True


def solve_sudoku(sudoku):
    if sudoku.size == np.count_nonzero(sudoku):
        return True

    for row in range(len(sudoku)):
        for col in range(len(sudoku)):
            if sudoku[row, col] == 0:
                for num in range(1, 10):
                    if is_valid(sudoku, num, row, col):
                        sudoku[row, col] = num
                        if solve_sudoku(sudoku):
                            return True
                        else:
                            sudoku[row, col] = 0
                return False


def compute():
    with open(data_dir + '\p096_sudoku.txt') as f:
        in_file = f.read().splitlines()

    # Store all sudokus as numpy arrays in a dictionary
    sudoku = np.zeros([9, 9])
    row = 0
    all_sudokus = dict()
    idx = 0
    for line in in_file:
        if 'Grid' in line:
            sudoku = np.zeros([9, 9])
            row = 0
        else:
            col = 0
            for val in line:
                sudoku[row, col] = int(val)
                col += 1
            row += 1

        if row == 9:
            all_sudokus[idx] = sudoku
            idx += 1

    num_sud = idx
    tot = 0
    for idx in range(num_sud):
        sudoku = all_sudokus[idx]
        solve_sudoku(sudoku)

        tot += (sudoku[0, 0] * 100 + sudoku[0, 1] * 10 + sudoku[0, 2])
        print(tot)
    return tot


if __name__ == '__main__':
    print(compute())
