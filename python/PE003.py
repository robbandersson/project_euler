#
# Solution to Project Euler problem 3 - Largest prime factor
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime
import math


def compute():
    num = 600851475143
    prime_list = []

    for i in range(2, int(math.ceil(math.sqrt(num)))+1):
        if is_prime(i):
            if num % i == 0:
                prime_list.append(i)
                num = num / i

    return max(prime_list)


if __name__ == '__main__':
    print(compute())
