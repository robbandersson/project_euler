#
# Solution to Project Euler problem 88 - Product-sum numbers
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import prime_factorize


def partition(collection):
    if len(collection) == 1:
        yield [collection]
        return

    first = collection[0]
    for smaller in partition(collection[1:]):
        # insert `first` in each of the subpartition's subsets
        for n, subset in enumerate(smaller):
            yield smaller[:n] + [[first] + subset] + smaller[n+1:]
        # put `first` in its own subset
        yield [[first]] + smaller


def compute():
    prod_sum = dict()
    upper = 12000
    n = 2

    while len(prod_sum) + 1 < upper:
        primes = prime_factorize(n)
        if len(primes) > 1:
            for part in partition(primes):
                if len(part) > 1:
                    s = 0
                    num_sub_part = 0
                    for sub_part in part:
                        prod = 1
                        for val in sub_part:
                            prod *= val
                        s += prod
                        num_sub_part += 1
                    k = num_sub_part + (n - s)
                    if k <= upper and ((k in prod_sum and prod_sum[k] > n) or k not in prod_sum):
                        prod_sum[k] = n
        n += 1
    return sum(set(prod_sum.values()))


if __name__ == '__main__':
    print(compute())
