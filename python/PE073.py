#
# Solution to Project Euler problem 73 - Counting fractions in a range
#
# https://gitlab.com/robbandersson/project_euler
#
import math
from src.functions import is_coprime


def compute():
    # Set count to -2 to account for 1/3 and 1/2, who are not in the set
    count = -2
    upper = 12000
    for d in range(2, upper + 1):
        lower = int(math.ceil(d / 3))
        upper = int(math.floor(d / 2))
        for n in range(lower, upper + 1):
            if is_coprime(n, d):
                count += 1
    return count


if __name__ == '__main__':
    print(compute())
