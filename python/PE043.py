#
# Solution to Project Euler problem 43 - Sub-string divisibility
#
# https://gitlab.com/robbandersson/project_euler
#
from itertools import permutations


def compute():
    ref_str = '0123456789'
    permute_list = [''.join(p) for p in permutations(ref_str)]

    tot = 0
    divis_list = [2, 3, 5, 7, 11, 13, 17]
    for p in permute_list:
        if p[0] != '0':
            has_property = True
            idx = 0
            for k in range(1, 8):
                if int(p[k:k+3]) % divis_list[idx] != 0:
                    has_property = False
                idx += 1
            if has_property:
                tot += int(p)

    return tot


if __name__ == '__main__':
    print(compute())
