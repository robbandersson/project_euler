#
# Solution to Project Euler problem 52 - Permuted multiples
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    flag = True
    num = 0
    while flag:
        num += 1
        inner_flag = True
        for mult in range(2, 7):
            if sorted(str(mult * num)) != sorted(str(num)):
                inner_flag = False 
                break
        if inner_flag:
            flag = False
        
    return num


if __name__ == '__main__':
    print(compute())
