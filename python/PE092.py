#
# Solution to Project Euler problem 92 - Square digit chain
#
# https://gitlab.com/robbandersson/project_euler
#


def ending_square_chain(n):
    next_num = 0
    for elem in str(n):
        next_num += int(elem) ** 2
    while next_num != 1 and next_num != 89:
        temp_num = 0
        for elem in str(next_num):
            temp_num += int(elem) ** 2
        next_num = temp_num
    return next_num


def compute():
    upper = 10000000
    n = 1
    count = 0

    while n < upper:
        if ending_square_chain(n) == 89:
            count += 1
        n += 1

    return count


if __name__ == '__main__':
    print(compute())
