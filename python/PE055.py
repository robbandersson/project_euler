#
# Solution to Project Euler problem 55 - Lychrel numbers
#
# https://gitlab.com/robbandersson/project_euler
#


def is_palindrome(n):
    return str(n) == str(n)[::-1]


def get_reversed(n):
    return int(str(n)[::-1])


def compute():
    num_lychrel = 0
    for i in range(10000):
        num_iter = 0
        lychrel = True
        num = i
        while num_iter < 50:
            num = num + get_reversed(num)
            if is_palindrome(num):
                lychrel = False
                break
            num_iter += 1
        if lychrel:
            num_lychrel += 1

    return num_lychrel

if __name__ == '__main__':
    print(compute())
