#
# Solution to Project Euler problem 5 - Smallest multiple
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import prime_factorize


def compute():
    full_prime_list = []
    for i in range(2, 21):
        factor_list = prime_factorize(i)
        for j in range(0, len(factor_list)):
            if factor_list[j] in full_prime_list:
                full_prime_list.remove(factor_list[j])
        full_prime_list += factor_list

    prod = 1
    for i in range(0, len(full_prime_list)):
        prod *= int(full_prime_list[i])

    return prod


if __name__ == '__main__':
    print(compute())
