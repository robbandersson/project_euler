#
# Solution to Project Euler problem 66 - Diophantine equation
#
# https://gitlab.com/robbandersson/project_euler
#
import math
import numpy as np


def compute():
    result = 0
    x_max = 0
    for D in range(2, 1000):
        a0 = int(math.sqrt(D))
        if a0 * a0 == D:
            continue
        else:
            m = 0
            d = 1
            a = a0

            num1 = 1
            num2 = a0

            den1 = 0
            den2 = 1

            while num2 * num2 - D * den2 * den2 != 1:
                m  = d * a - m
                d = (D - m ** 2) / d
                a = int(np.floor((a0 + m) / d))

                num0 = int(num1)
                num1 = int(num2)
                num2 = int(a * num1 + num0)

                den0 = int(den1)
                den1 = int(den2)
                den2 = int(a * den1 + den0)

            if num2 > x_max:
                x_max = num2
                result = D

    return result


if __name__ == '__main__':
    print(compute())
