#
# Solution to Project Euler problem 56 - Powerful digit sum
#
# https://gitlab.com/robbandersson/project_euler
#


def digital_sum(a, b):
    pow_num = str(a ** b)
    tot = 0
    for i in range(len(pow_num)):
        tot += int(pow_num[i])
    return tot


def compute():
    max_digital_sum = 0
    for a in range(1, 100):
        for b in range(1, 100):
            sum_a_b = digital_sum(a, b)
            if sum_a_b > max_digital_sum:
                max_digital_sum = sum_a_b
    return max_digital_sum

if __name__ == '__main__':
    print(compute())
