#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def compute():
    x = 2
    count = 0
    target = 1000000

    while count < target:
        x += 1
        for yz in range(2, 2 * x):
            dist = math.sqrt(x ** 2 + yz ** 2)
            if dist == int(dist):
                if yz <= x:
                    count += yz // 2
                else:
                    count += (1 + x - (yz + 1) // 2)
    return x


if __name__ == '__main__':
    print(compute())
