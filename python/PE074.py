#
# Solution to Project Euler problem 74 - Digit factorial range
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def next_in_factorial_chain(n):
    num = 0
    s = str(n)
    for i in range(len(s)):
        num += math.factorial(int(s[i]))
    return num


def compute():
    upper = 1000000
    tot = 0
    for i in range(1, upper + 1):
        factorial_chain = set()
        num = i
        while num not in factorial_chain:
            factorial_chain.add(num)
            num = next_in_factorial_chain(num)
        if len(factorial_chain) == 60:
            tot += 1
    return tot


if __name__ == '__main__':
    print(compute())