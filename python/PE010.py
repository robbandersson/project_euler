#
# Solution to Project Euler problem 10 - Summation of primes
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    tot = 0
    for i in range(2, 2000000):
        if is_prime(i):
            tot += i
    return tot


if __name__ == '__main__':
    print(compute())
