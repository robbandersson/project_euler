#
# Solution to Project Euler problem 46 - Goldbach's other conjecture
#
# https://gitlab.com/robbandersson/project_euler
#
import math
from src.functions import is_prime


def is_square(x):
    return int(math.sqrt(x)) == math.sqrt(x)


def conjecture(x, prime_list):
    for i in range(len(prime_list)):
        if is_square((x - int(prime_list[i])) / 2):
            return True
    return False


def compute():
    odd_comp_num = 3
    prime_list = [2]
    while True:
        if is_prime(odd_comp_num):
            prime_list.append(odd_comp_num)
            odd_comp_num += 2
        elif conjecture(odd_comp_num, prime_list):
            odd_comp_num += 2
        else:
            break

    return odd_comp_num


if __name__ == '__main__':
    print(compute())
