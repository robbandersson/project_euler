#
# Solution to Project Euler problem 47 - Distinct prime factors
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import prime_factorize


def compute():
    consecutive = list()
    num = 2
    while len(consecutive) < 4:
        if len(set(prime_factorize(num))) == 4:
            consecutive.append(num)
        else:
            consecutive = list()
        num += 1

    return consecutive[0]


if __name__ == '__main__':
    print(compute())
