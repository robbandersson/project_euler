#
# Solution to Project Euler problem 82 - Path sum: three ways
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import numpy as np
data_dir = os.path.normpath("..\data")


def load_matrix(name):
    with open(data_dir + name) as f:
        lines = [list(map(int, line.split(','))) for line in f]
    return lines


def compute():
    matrix = load_matrix('\p082_matrix.txt')
    #matrix = np.array([[131, 673, 234, 103, 18], [201, 96, 342, 965, 150], [630, 803, 746, 422, 111],
     #                  [537, 699, 497, 121, 956], [805, 732, 524, 37, 331]])
    mini_path = np.zeros([len(matrix), len(matrix)])
    for k in range(len(mini_path)):
        mini_path[k][0] = matrix[k][0]

    keys = list(range(len(mini_path)))
    for k in range(1, len(mini_path)):
        values = mini_path[:, k - 1]
        paths = dict(zip(keys, values))

        while np.count_nonzero(mini_path[:, k]) < len(mini_path):
            pos = min(paths, key=paths.get)
            if mini_path[pos][k] == 0:
                mini_path[pos][k] = matrix[pos][k] + mini_path[pos][k - 1]
                paths[pos] = matrix[pos][k] + mini_path[pos][k - 1]
            else:
                if pos == 0:
                    if mini_path[pos + 1][k] == 0:
                        mini_path[pos + 1][k] = matrix[pos + 1][k] + mini_path[pos][k]
                        paths[pos + 1] = matrix[pos + 1][k] + mini_path[pos][k]
                        paths.pop(pos, None)
                    else:
                        paths.pop(pos, None)
                elif pos == len(mini_path) - 1:
                    if mini_path[pos - 1][k] == 0:
                        mini_path[pos - 1][k] = matrix[pos - 1][k] + mini_path[pos][k]
                        paths[pos - 1] = matrix[pos - 1][k] + mini_path[pos][k]
                        paths.pop(pos, None)

                    else:
                        paths.pop(pos, None)
                else:
                    if mini_path[pos - 1][k] == 0:
                        mini_path[pos - 1][k] = matrix[pos - 1][k] + mini_path[pos][k]
                        paths[pos - 1] = matrix[pos - 1][k] + mini_path[pos][k]
                        paths.pop(pos, None)

                    else:
                        paths.pop(pos, None)
                    if mini_path[pos + 1][k] == 0:
                        mini_path[pos + 1][k] = matrix[pos + 1][k] + mini_path[pos][k]
                        paths[pos + 1] = matrix[pos + 1][k] + mini_path[pos][k]
                        paths.pop(pos, None)

                    else:
                        paths.pop(pos, None)

    return min(mini_path[:, len(mini_path) - 1])


if __name__ == '__main__':
    print(compute())
