#
# Solution to Project Euler problem 243 - Resilience
#
# https://gitlab.com/robbandersson/project_euler
#
# The problem is solved in two steps:
# 1) Find the the highest denominator, constructed by multiplying the first n distinct primes, that does not satisfy
# the inequality
# 2) Multiply the found denominator by every number up to the next prime, until we found one that satisfies the
# inequality
from src.functions import is_prime, euler_totient
from decimal import *
getcontext().prec = 40


def compute():
    threshold = Decimal(15499) / Decimal(94744)
    denominator = 2
    found = False
    num = 3
    primes = [2]
    while not found:
        if is_prime(num):
            denominator *= num
            primes.append(num)
        else:
            num += 2
            continue
        if Decimal(euler_totient(denominator)) / Decimal(denominator - 1) < threshold:
            found = True
            denominator /= num
        else:
            num += 2

    test_num = primes
    for i in range(4, max(primes), 2):
        test_num.append(i)
    test_num = sorted(test_num)

    for num in test_num:
        test_denominator = denominator * num
        if Decimal(euler_totient(test_denominator)) / Decimal(test_denominator - 1) < threshold:
            denominator = test_denominator
            break

    return int(denominator)


if __name__ == '__main__':
    print(compute())
