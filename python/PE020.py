#
# Solution to Project Euler problem 20 - Factorial digital sum
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def get_digit(number, n):
    return number // 10 ** n % 10


def compute():
    fac = int(input("State the factorial:"))
    num = math.factorial(fac)
    tot = 0
    for i in range(int(math.log10(num)) + 1):
        tot += get_digit(num, i)

    return tot


if __name__ == '__main__':
    print(compute())
