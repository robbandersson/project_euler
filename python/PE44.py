#
# Solution to Project Euler problem 44 -
#
# https://gitlab.com/robbandersson/project_euler
#
def isPentagonal(sum,n):
    pentNbr = (n * (3 * n - 1) / 2)
    while pentNbr<=sum:
        n+=1
        pentNbr=(n * (3 * n - 1) / 2)
    if sum==pentNbr:
        return True
    else:
        return False


pentList=[]
n=1
loopBreaker=False
while True:
    pentNbr = (n * (3 * n - 1) / 2)
    for i in range(len(pentList)-1,0,-1):
        pentDiff = pentNbr-pentList[i]
        print(pentDiff)
        if pentDiff in pentList and isPentagonal(pentNbr+pentList[i],n+1):
            print(pentDiff)
            loopBreaker=True
            break
    if loopBreaker:
        break
    pentList.append(pentNbr)
    n+=1
