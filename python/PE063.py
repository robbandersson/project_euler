#
# Solution to Project Euler problem 63 - Powerful digit counts
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    tot = 0
    for b in range(1, 60):
        for a in range(1, 10):
            num = a ** b
            if len(str(num)) == b:
                tot += 1
                print(a, b, num)

    return tot


if __name__ == '__main__':
    print(compute())
