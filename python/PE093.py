#
# Solution to Project Euler problem 93 - Arithmetic expressions
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools


def solve_eq(eq_list, order_of_operation):
    if not order_of_operation:
        return eq_list[0]

    if order_of_operation[0] == 'FIRST':
        idx = 1
    elif order_of_operation[0] == 'SECOND':
        idx = 3
    else:
        idx = 5
    operation = eq_list[idx]

    if operation == 'MULT':
        upd_eq_list = eq_list[0:idx-1] + [eq_list[idx - 1] * eq_list[idx + 1]] + eq_list[idx + 2:]
        return solve_eq(upd_eq_list, order_of_operation[1:])

    elif operation == 'DIV':
        if eq_list[idx + 1] != 0:
            upd_eq_list = eq_list[0:idx-1] + [eq_list[idx - 1] / eq_list[idx + 1]] + eq_list[idx + 2:]
            return solve_eq(upd_eq_list, order_of_operation[1:])
        else:
            return 0

    elif operation == 'ADD':
        upd_eq_list = eq_list[0:idx-1] + [eq_list[idx - 1] + eq_list[idx + 1]] + eq_list[idx + 2:]
        return solve_eq(upd_eq_list, order_of_operation[1:])

    elif operation == 'SUB':
        upd_eq_list = eq_list[0:idx-1] + [eq_list[idx - 1] - eq_list[idx + 1]] + eq_list[idx + 2:]
        return solve_eq(upd_eq_list, order_of_operation[1:])


def compute():
    result = ''
    num_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ]
    operations = ['DIV', 'MULT', 'ADD', 'SUB', ]
    longest_consecutive = 0
    for comb in itertools.combinations(num_list, 4):
        generated_num = set()
        for order in itertools.permutations(comb):
            for operation_permute in itertools.product(operations, repeat=3):
                eq_list = [None] * (len(order) + len(operation_permute))
                eq_list[::2] = order
                eq_list[1::2] = operation_permute
                for operation_order in itertools.product(['FIRST', 'SECOND', 'THIRD'], repeat=3):
                    if (not operation_order[2] == 'THIRD' and
                            not operation_order[1] == 'THIRD' and
                            not operation_order[2] == 'SECOND'):
                        m = solve_eq(eq_list, operation_order)
                        if int(m) == m and m > 0:
                            generated_num.add(m)
        n = 1
        while n in generated_num:
            n += 1
        n -= 1

        if n > longest_consecutive:
            longest_consecutive = n
            result = (''.join(str(e) for e in sorted(comb)))

    return result, longest_consecutive


if __name__ == '__main__':
    print(compute())
