#
# Solution to Project Euler problem 14 - Longest Collatz sequence
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    chain_len = 0
    chain_start = 0
    limit = 1000000
    for i in range(1, limit):
        num = i
        seq = [num]
        while num != 1:
            if num % 2 == 0:
                num = num / 2
                seq.append(int(num))
            else:
                num = 3 * num + 1
                seq.append(int(num))
        if len(seq) > chain_len:
            chain_len = len(seq)
            chain_start = i

    return chain_start


if __name__ == '__main__':
    print(compute())
