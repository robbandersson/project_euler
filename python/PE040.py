#
# Solution to Project Euler problem 40 - Champernowne's constant
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    num = 1
    l = []
    while len(l) < 1000001:
        temp_str = str(num)
        for i in range(len(temp_str)):
            l.append(temp_str[i])
        num += 1

    prod = int(l[9]) * int(l[99]) * int(l[999]) * int(l[9999]) * int(l[99999]) * int(l[999999])
    return prod


if __name__ == '__main__':
    print(compute())
