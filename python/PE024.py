#
# Solution to Project Euler problem 24 - Lexiographic permutations
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools as it


def compute():
    iter_list = list(it.permutations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]))
    lst_str = ''.join(map(str, iter_list[999999]))
    return lst_str


if __name__ == '__main__':
    print(compute())
