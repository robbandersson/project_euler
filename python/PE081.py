#
# Solution to Project Euler problem 81 - Path sum: two ways
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import numpy as np
data_dir = os.path.normpath("..\data")


def load_matrix(name):
    with open(data_dir + name) as f:
        lines = [list(map(int, line.split(','))) for line in f]
    return lines


def compute():
    matrix = load_matrix('\p081_matrix.txt')
    mini_path = np.zeros([len(matrix), len(matrix)])
    mini_path[0][0] = matrix[0][0]
    # Fill first row and first column in mini_path
    for k in range(1, len(mini_path)):
        mini_path[0][k] = matrix[0][k] + mini_path[0][k - 1]
        mini_path[k][0] = matrix[k][0] + mini_path[k - 1][0]

    n = 1
    while n < len(matrix):
        for k in range(1, n):
            mini_path[n][k] = matrix[n][k] + min(mini_path[n][k - 1], mini_path[n - 1][k])
            mini_path[k][n] = matrix[k][n] + min(mini_path[k][n - 1], mini_path[k - 1][n])

        mini_path[n][n] = matrix[n][n] + min(mini_path[n - 1][n], mini_path[n][n - 1])
        n += 1

    return mini_path[n - 1][n - 1]


if __name__ == '__main__':
    print(compute())
