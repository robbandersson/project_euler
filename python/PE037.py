#
# Solution to Project Euler problem  - 
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    num_primes = 0
    num = 11
    tot = 0
    while num_primes < 11:
        if is_prime(num):
            prime_flag = True
            s_num = str(num)
            for k in range(1, len(s_num)):
                if not is_prime(int(s_num[k::])) or not is_prime(int(s_num[0:(len(s_num) - k)])):
                    prime_flag = False
            if prime_flag:
                num_primes += 1
                tot += num

        num += 2
    return tot


if __name__ == '__main__':
    print(compute())
