#
# Solution to Project Euler problem 5 - Sum square difference
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def compute():
    sum1 = 0
    sum2 = 0
    for i in range(1, 101):
        sum1 += math.pow(i, 2)
        sum2 += i

    return math.pow(sum2, 2) - sum1


if __name__ == '__main__':
    print(compute())
