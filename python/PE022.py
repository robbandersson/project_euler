#
# Solution to Project Euler problem 22 - Names score
#
# https://gitlab.com/robbandersson/project_euler
#
from string import ascii_uppercase
import os
data_dir = os.path.normpath('data')


def compute():
    LETTERS = {letter: str(index) for index, letter in enumerate(ascii_uppercase, start=1)}
    print(LETTERS)
    filename = data_dir + '/p022_names.txt'
    name_list = open(filename, 'r+')
    name_list = name_list.read()

    name_list = name_list.split(',')
    name_list = sorted(name_list)

    i = 1
    tot_sum = 0
    for names in name_list:
        names = names.replace("\"", "")
        tot = 0
        for letter in names:
            tot += int(LETTERS[letter])
        tot_sum += tot * i
        i += 1

    return tot_sum


if __name__ == '__main__':
    print(compute())
