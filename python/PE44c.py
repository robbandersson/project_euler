#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
def isPent(x):
    for i in range(len(pentList)-1):
        if 2*pentList[i]-x in pentList and x-pentList[i] in pentList:
            return 2*pentList[i]-x
    return 0

pentList=[]
n=1
while True:
    pentNbr = (n * (3 * n - 1) / 2)
    pentList.append(pentNbr)
    if isPent(pentNbr)>0:
        print(isPent(pentNbr))
        break
    n+=1



