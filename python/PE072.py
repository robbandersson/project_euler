#
# Solution to Project Euler problem 72 - Counting fractions
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import euler_totient


def compute():
    upper = 1000001
    num_frac = 0
    for d in range(2, upper):
        num_frac += euler_totient(d)
    return num_frac


if __name__ == '__main__':
    print(compute())
