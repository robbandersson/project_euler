#
# Solution to Project Euler problem 29 - Distinct power
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    l = []
    for a in range(2, 101):
        for b in range(2, 101):
            l.append(pow(a, b))

    l = list(set(l))
    return len(l)


if __name__ == '__main__':
    print(compute())
