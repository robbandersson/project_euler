#
# Solution to Project Euler problem 27 - Quadratic prime
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def find_prim_factors(num):
    """Create prime factorization of an integer

    Args:
        num (int): integer to factorized into primes

    Returns:
        factors (list): list of prime factors of num

    """
    i = 2
    factors = []
    while num % i == 0:
        num = num / i
        factors.append(i)
    i = 3
    while i * i <= num:
        while num % i == 0:
            num = num / i
            factors.append(i)
        i += 2
    if num > 1:
        factors.append(int(num))
    return factors


def compute():
    list_of_params = []
    for a in range(-1000, 1000):
        for b in range(-1000, 1000):
            n = 0
            while is_prime(n ** 2 + a * n + b):
                n += 1
            list_of_params.append([n, a, b])

    max_value = max(list_of_params)
    max_index = list_of_params.index(max_value)

    print(max_value)
    return list_of_params[max_index][1] * list_of_params[max_index][2]


if __name__ == '__main__':
    print(compute())
