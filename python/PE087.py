#
# Solution to Project Euler problem 87 - Prime power triples
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime
import math

def compute():
    upper = 50000000
    upper_square = int(math.sqrt(upper))
    upper_cube = int(upper ** (1 / 3))
    upper_fourth = int(upper ** (1 / 4))
    combinations = set()

    for i in range(2, upper_square):
        if is_prime(i):
            for j in range(2, upper_cube):
                if is_prime(j):
                    for k in range(2, upper_fourth):
                        if is_prime(k):
                            s = i ** 2 + j ** 3 + k ** 4
                            if s < upper:
                                combinations.add(s)

    return len(combinations)


if __name__ == '__main__':
    print(compute())
