#
# Solution to Project Euler problem 53 - Combinatoric selections
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def nCr(n, r):
    return math.factorial(n) / (math.factorial(r) * math.factorial(n - r))


def compute():
    num = 0
    for n in range(1, 101):
        UPPER = n // 2
        for r in range(1, UPPER):
            if nCr(n, r) > 1000000:
                if n & 2 == 0:
                    num += 2 * (UPPER - r) + 1
                    break
                else:
                    num += 2 * (UPPER - r) + 2
                    break
    return num


if __name__ == '__main__':
    print(compute())
