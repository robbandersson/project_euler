#
# Solution to Project Euler problem 33 - Digit cancelling fractions
#
# https://gitlab.com/robbandersson/project_euler
#


def get_digit(num, n):
    return num // 10**n % 10


def is_cancel_frac(num, den):
    if (num % 10 == 0) and (den % 10 == 0):
        return False

    for k in range(0, 2):
        for l in range(0, 2):
            if get_digit(num, k) == get_digit(den, l):
                new_num = get_digit(num, 1-k)
                new_den = get_digit(den, 1-l)
                if new_den != 0:
                    if new_num / new_den == num / den:
                        return True
    return False


def compute():
    prod_den = 1
    prod_num = 1
    for den in range(11, 100):
        for num in range(11, den):
            if is_cancel_frac(num, den):
                prod_num = prod_num * num
                prod_den = prod_den * den

    return prod_num, prod_den


if __name__ == '__main__':
    print(compute())
