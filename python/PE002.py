#
# Solution to Project Euler problem 2 - Even Fibonacci numbers
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    fib_list = [1, 2]
    tot = 2
    while 1:
        fib_list.append(fib_list[-1] + fib_list[-2])
        if fib_list[-1] > 4000000:
            break
        elif fib_list[-1] % 2 == 0:
            tot += fib_list[-1]
    return tot


if __name__ == '__main__':
    print(compute())
