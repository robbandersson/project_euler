#
# Solution to Project Euler problem 51 - Prime digit replacement
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime
from itertools import combinations

def compute():
    primes = {2}
    found_prime_family = False
    n = 3
    while not found_prime_family:
        # Check if n is prime
        prime_flag = False
        if n not in primes:
            if is_prime(n):
                primes.add(n)
                prime_flag = True
        else:
            prime_flag = True

        # If n is prime, check all replacement permutations
        if prime_flag:
            s = str(n)
            ref = list(range(len(s)))
            for k in range(1, len(s)):
                for perm in combinations(ref, k):
                    num_primes = 0
                    family = list()
                    for i in range(10):
                        new_s = s
                        for idx in perm:
                            new_s = new_s[:idx] + str(i) + new_s[idx + 1:]
                        if len(str(int(new_s))) == len(s):
                            if int(new_s) in primes:
                                num_primes += 1
                                family.append(int(new_s))
                            elif is_prime(int(new_s)):
                                primes.add(int(new_s))
                                num_primes += 1
                                family.append(int(new_s))

                    if num_primes == 8:
                        return min(family)
        n += 2

if __name__ == '__main__':
    print(compute())
