#
# Solution to Project Euler problem 101 - Optimum Polynomial
#
# https://gitlab.com/robbandersson/project_euler
#

def compute():
    coeffs = [1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1]
    #coeffs = [0, 0, 0, 1]
    X, Y = [], []
    for i in range(len(coeffs)):
        X.append(i + 1)
        Y.append(poly_eval(coeffs, i + 1))    
    tot = 0
    for i in range(len(X)):
        tot += lagrange_polynomial(X[:i], Y[:i])

    return tot


def poly_eval(coeffs, x):
    tot = 0
    for i, coeff in enumerate(coeffs):
        tot += coeff * x ** i

    return tot


def lagrange_polynomial(X, Y):    
    k = len(X)
    tot = 0
    for j in range(k):
        prod = 1
        y = Y[j]
        for m in range(k):
            if m != j:
                prod *= ((k + 1) - X[m]) / (X[j] - X[m])
        tot += y * prod
       
    return tot


if __name__ == '__main__':
    print(compute())
