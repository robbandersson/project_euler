#
# Solution to Project Euler problem 108 - Diophantine Reciprocals I
#
# https://gitlab.com/robbandersson/project_euler
#
import os, sys
import math
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), "../src"))
from functions import prime_factorize, is_prime


def num_squared_divisors(prime_factors):
    num = 1
    for prime in set(prime_factors):
        num *= (1 + 2 * prime_factors.count(prime))
    return num


def get_next_prime(prime):
    if prime == 2:
        return 3
    else:
        p = prime + 2
        while not is_prime(p):
            p += 2
        return p
    

def compute():
    threshold = 1000
    max_prime_idx = math.ceil(math.log(threshold) / math.log(3))
    p_list = []
    prime = 2
    while len(p_list) < max_prime_idx:
        p_list.append(prime)
        prime = get_next_prime(prime)
    
    threshold = 1000
    n = 2
    while (num_squared_divisors(prime_factorize(n)) + 1) / 2 < threshold:
        n += 1
    
    return n
    
if __name__ == '__main__':
    print(compute())
