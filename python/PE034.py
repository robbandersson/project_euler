#
# Solution to Project Euler problem 34 - Digit factorials
#
# https://gitlab.com/robbandersson/project_euler
#

import math


def get_digit(num, n):
    return num // 10**n % 10


def compute():
    tot = 0
    for num in range(3, 2540160):
        fac_sum = 0
        for j in range(len(str(num))):
            fac_sum += math.factorial(get_digit(num, j))
        if fac_sum == num:
            tot += fac_sum

    return tot


if __name__ == '__main__':
    print(compute())
