#
# Solution to Project Euler problem 7 - 10001st prime
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    count = 0
    prime_candidate = 1
    while count < 10001:
        prime_candidate += 1
        if is_prime(prime_candidate):
            count += 1
    return prime_candidate


if __name__ == '__main__':
    print(compute())
