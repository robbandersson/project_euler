#
# Solution to Project Euler problem 77 - Prime summations
#
# https://gitlab.com/robbandersson/project_euler
#
# Solved using the Euler transform - https://mathworld.wolfram.com/EulerTransform.html

from src.functions import is_prime


def sum_of_prime_factors(n):
    tot = 0
    p = 2
    while p <= n:
        if is_prime(p):
            if n % p == 0:
                tot += p
        p += 1
    return tot


def compute():
    limit = 5000
    prime_partitions = dict()
    prime_partitions[1] = 0
    n = 1
    while prime_partitions[n] < limit:
        n += 1
        inner_sum = 0
        for i in range(1, n):
            inner_sum += sum_of_prime_factors(i) * prime_partitions[n - i]
        prime_partitions[n] = int((sum_of_prime_factors(n) + inner_sum) / n)
    return n


if __name__ == '__main__':
    print(compute())
