#
# Solution to Project Euler problem 49 - Prime permutations
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools
from src.functions import is_prime


def n_filter(filters, tuples):
    for f in filters:
        tuples = filter(f, tuples)
    return list(tuples)


def remove_duplicates(in_list):
    out_list = []
    for element in in_list:
        if element not in out_list:
            out_list.append(element)
    return out_list


def find_permute(x):
    l = itertools.permutations(str(x))
    permutations = [int(''.join(z)) for z in l]
    return permutations


def exist_arith_seq(x):
    l = list(itertools.permutations(x, 3))
    for i in range(len(l)):
        sub_list = sorted(l[i])
        if sub_list[1] - sub_list[0] == sub_list[2] - sub_list[1]:
            return sub_list, True
    return [], False


def compute():
    prime_list = []
    for i in range(1001, 10000, 2):
        if is_prime(i):
            prime_list.append(i)

    filters = [lambda x: x > 1000,lambda x: is_prime(x) == True, lambda x: x != 1487, lambda x: x != 4817, lambda x: x != 8147]
    for i in range(len(prime_list)):
        permute_list = remove_duplicates(n_filter(filters, find_permute(prime_list[i])))
        if len(permute_list) > 2:
            (l, boolean) = exist_arith_seq(permute_list)
            if boolean:
                break

    return ''.join(map(str, l))


if __name__ == '__main__':
    print(compute())
