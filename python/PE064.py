#
# Solution to Project Euler problem 64 - Odd period square roots
#
# https://gitlab.com/robbandersson/project_euler
#
import numpy as np


def is_square(n):
    return int(np.sqrt(n) + 0.5) ** 2 == n


def generate_an(num, den, root):
    new_den = root - den ** 2
    new_num = np.abs(den)
    if num != 1:
        new_den = new_den / num

    i = 0
    while np.sqrt(root) + new_num > new_den:
        new_num = new_num - new_den
        i += 1

    return i, int(new_num), int(new_den)


def compute():
    num_odd = 0
    latest_square_root = 1
    for n in range(2, 10001):
        if not is_square(n):
            a0 = latest_square_root
            i, num, den = generate_an(1, a0, n)
            seq = list()
            seq.append([i, num, den])
            i_new, num_new, den_new = generate_an(den, num, n)
            while [i_new, num_new, den_new] not in seq:
                seq.append([i_new, num_new, den_new])
                i_new, num_new, den_new = generate_an(den_new, num_new, n)
            if len(seq) % 2 != 0:
                num_odd += 1
        else:
            latest_square_root += 1
    return num_odd


if __name__ == '__main__':
    print(compute())

