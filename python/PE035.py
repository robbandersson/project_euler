#
# Solution to Project Euler problem 30 - Circular primes
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def rotate(num, k):
    str_num = str(num)
    return int(str_num[k:] + str_num[:k])


def get_all_rotations(num):
    all_rotations = set()
    for k in range(len(str(num))):
        all_rotations.add(rotate(num, k))
    return all_rotations


def compute():
    tot_circ_prime = 0
    for num in range(2, 1000000):
        if is_prime(num):
            all_rotations = get_all_rotations(num)
            circ_prime = True
            for val in all_rotations:
                if not is_prime(val):
                    circ_prime = False
                    break
            if circ_prime:
                tot_circ_prime += 1

    return tot_circ_prime


if __name__ == '__main__':
    print(compute())
