#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
def pascal_triangle(rows, divisor):
    n = 1
    total = 0
    while n < rows:
        total += ((n - 1) // divisor + 1) * ((n - 1) % divisor + 1)
        print(total, ((n - 1) // divisor + 1) * ((n - 1) % divisor + 1))
        n += 1
    return total

pascal = pascal_triangle(100, 7)
print(pascal)