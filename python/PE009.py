#
# Solution to Project Euler problem 9 - Special Pythagorean triplet
#
# https://gitlab.com/robbandersson/project_euler
#
import math


def compute():
    prod = 0
    for a in range(1, 334):
        for b in range(a, 1000 - a):
            num = math.sqrt(pow(a, 2) + pow(b, 2)) + b + a
            if num == 1000:
                prod = a * b * (1000 - a - b)
    return prod


if __name__ == '__main__':
    print(compute())
