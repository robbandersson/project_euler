#
# Solution to Project Euler problem 751 - Concatenation coincidence
#
# https://gitlab.com/robbandersson/project_euler
#
import numpy as np
from decimal import *
getcontext().prec = 40


def a_from_theta(theta, n):
    b = np.array([np.float(theta)])
    idx = 1
    while idx < n:
        b = np.append(b, np.floor(b[idx-1]) * (b[idx-1] - np.floor(b[idx-1]) + 1))
        idx += 1
    return int(np.floor(b[-1]))


def to_concat_seq(arr):
    concat_seq = '2.'
    for n in range(1, len(arr)):
        concat_seq += str(arr[n])
    return Decimal(concat_seq)


def compute():
    a_arr = np.array([2])
    next_val = 2
    while len(str(to_concat_seq(a_arr))) < 26:
        a_arr_upd = np.append(a_arr, next_val)
        while a_from_theta(to_concat_seq(a_arr_upd), len(a_arr_upd)) != next_val:
            next_val += 1
            a_arr_upd = np.append(a_arr, next_val)
        a_arr = a_arr_upd

    return to_concat_seq(a_arr)


if __name__ == '__main__':
    print(compute())
