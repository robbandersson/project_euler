#
# Solution to Project Euler problem 15 - Lattice paths
#
# https://gitlab.com/robbandersson/project_euler
#


def binomial(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke.
    See http://stackoverflow.com/questions/3025162/statistics-combinations-in-python
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0


def compute():
    max_grid = int(input("Enter a max size of your grid:"))
    grid = [[1 for i in range(max_grid + 1)] for j in range(max_grid + 1)]
    for i in range(1, max_grid + 1):
        for j in range(1, max_grid + 1):
            grid[i][j] = int(grid[i - 1][j]) + int(grid[i][j - 1])
    return grid[max_grid][max_grid]


if __name__ == '__main__':
    print(compute())
