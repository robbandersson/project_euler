#
# Solution to Project Euler problem 26 - Reciprocal cycles
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    max_len = 0
    longest_prime = 0
    for i in range(2, 1000):
        if is_prime(i):
            if (1000 % i != 0):
                mult = 1000 % i
                curr_len = 1
                while(mult != 1):
                    mult = (mult * 1000) % i
                    curr_len += 1
                if curr_len > max_len:
                    longest_prime = i
                    max_len = curr_len

    print(longest_prime, max_len)


if __name__ == '__main__':
    print(compute())
