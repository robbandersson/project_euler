#
# Solution to Project Euler problem 110 - Diophantine Reciprocals II
#
# https://gitlab.com/robbandersson/project_euler
#
import os, sys
import math
import numpy as np
import itertools as it
sys.path.append(os.path.join(os.path.dirname(__file__), "../src"))
from functions import prime_factorize, is_prime


def num_squared_divisors(prime_factors):
    num = 1
    for prime in set(prime_factors):
        num *= (1 + 2 * prime_factors.count(prime))
    return num


def get_next_prime(prime):
    if prime == 2:
        return 3
    else:
        p = prime + 2
        while not is_prime(p):
            p += 2
        return p
    

def compute():
    threshold = 4000000
    k = math.ceil(math.log(2 * threshold - 1) / math.log(3))
    prime_list = []
    prime = 2
    for i in range(k):
        prime_list.append(prime)
        prime = get_next_prime(prime)

    flag = True
    while flag:
        flag = False
        max_prime = max(prime_list)
        for k in range(2, max_prime):
            factors = prime_factorize(k)
            alt_prime_list = prime_list.copy()
            alt_prime_list.remove(max_prime)
            alt_prime_list.extend(factors)
            if num_squared_divisors(alt_prime_list) >= (2 * threshold - 1):
                prime_list = alt_prime_list
                flag = True
                break

    return math.prod(prime_list)
    
if __name__ == '__main__':
    print(compute())
