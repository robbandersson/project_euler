#
# Solution to Project Euler problem 62 - Cubic permutations
#
# https://gitlab.com/robbandersson/project_euler
#
import collections


def calc_cube(n):
    return n ** 3


def is_permutation(num_1, num_2):
    d = collections.defaultdict(int)
    for x in str(num_1):
        d[x] += 1
    for x in str(num_2):
        d[x] -= 1
    return not any(d.values())


def compute():
    found = False
    cubes = list()
    n = 1
    num_perm = 5
    while not found:
        permutation_list = list()
        next_cube = calc_cube(n)
        count = 1
        for cube in cubes:
            if is_permutation(next_cube, cube):
                permutation_list.append(cube)
                count += 1

        if count >= num_perm:
            return min(permutation_list)
        cubes.append(next_cube)
        n += 1


if __name__ == '__main__':
    print(compute())
