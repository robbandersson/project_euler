#
# Solution to Project Euler problem 105 - Minimal Network
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import math
file_path = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("..\data"))

def compute():
    with open(data_dir + '\p105_network.txt') as f:
        input = f.read()
    edges = {}
    graph = {}
    tot_weight = 0
    for i, line in enumerate(input.split("\n")):
        line = line.split(",")
        #connections = list()
        for j, elem in enumerate(line):
            if elem.isnumeric():
                if i in graph:
                    graph[i].append(j)
                else:
                    graph[i] = [j]
                edges[(i, j)] = int(elem)
                tot_weight += int(elem)

    tot_weight /= 2

    visited = [0]
    new_weight = 0
    while len(visited) < len(graph.keys()):
        shortest = 0
        for node in visited:
            for connected in graph[node]:
                if connected not in visited:
                    if shortest != 0: 
                        if edges[(node, connected)] < shortest:
                            shortest = edges[(node, connected)]
                            candidate = connected
                    else:
                        shortest = edges[(node, connected)]
                        candidate = connected
        new_weight += shortest
        visited.append(candidate)

    return tot_weight - new_weight

if __name__ == '__main__':
    print(compute())
