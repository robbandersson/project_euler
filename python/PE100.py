#
# Solution to Project Euler problem 100 - Arranged probability
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    lower = 1000000000000

    # Fundamental solution
    x1 = 3
    r1 = 1
    n = 8

    r = r1
    x = x1
    print('Number blue:', (2 * r + 1 + x) / 2, 'total number:', (2 * r + 1 + x) / 2 + r)
    while ((2 * r + 1 + x) / 2 + r) < lower:
        x_next = x1 * x + n * r1 * r
        r_next = x1 * r + r1 * x

        r = r_next
        x = x_next
        print('Number blue:', (2 * r + 1 + x) / 2, 'total number:', (2 * r + 1 + x) / 2 + r)

    return (2 * r + 1 + x) / 2


if __name__ == '__main__':
    print(compute())
