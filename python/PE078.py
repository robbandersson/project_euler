#
# Solution to Project Euler problem 78 - Coin partitions
#
# https://gitlab.com/robbandersson/project_euler
#
def first_pentagonal(n):
    return int(n * (3 * n - 1) / 2)


def second_pentagonal(n):
    return int(n * (3 * n + 1) / 2)


def compute():
    partitions = dict()
    partitions[0] = 1
    partitions[1] = 1
    partitions[2] = 2
    n = 2
    while partitions[n] % 1000000 != 0:
        n += 1
        pn = 0
        k = 1
        while n - first_pentagonal(k) >= 0:
            pn += (-1) ** (k - 1) * partitions[n - first_pentagonal(k)]
            k += 1

        k = 1
        while n - second_pentagonal(k) >= 0:
            pn += (-1) ** (k - 1) * partitions[n - second_pentagonal(k)]
            k += 1

        partitions[n] = pn


    return n, partitions[n]


if __name__ == '__main__':
    print(compute())