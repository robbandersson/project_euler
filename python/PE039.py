#
# Solution to Project Euler problem 39 - Integer right triangles
#
# https://gitlab.com/robbandersson/project_euler
#
import numpy as np


def compute():
    max_tri = 0
    perim = 0
    max_perim = 0
    for perim in range(5, 1001):
        num_tri = 0
        for a in range(1, int(np.floor(perim / (2 + np.sqrt(2))))):
            for b in range(a, int(np.floor(perim / 2))):
                c = perim - a - b
                if c > b:
                    if a ** 2 + b ** 2 == c ** 2:
                        num_tri += 1
        if num_tri > max_tri:
            max_tri = num_tri
            max_perim = perim
    print(max_perim)


if __name__ == '__main__':
    print(compute())
