#
# Solution to Project Euler problem 80 - Square root digital expansion
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_square
from decimal import *
getcontext().prec = 110


def compute():
    limit = 100
    tot = 0
    for n in range(2, limit + 1):
        if is_square(n):
            continue
        root_num = Decimal(n).sqrt()
        tot += (sum([int(i) for i in str(root_num)[2:101]]) + int(str(root_num)[0]))

    return tot


if __name__ == '__main__':
    print(compute())
