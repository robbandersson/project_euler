#
# Solution to Project Euler problem 45 - Triangular, pentagonal, and hexagonal
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    i = 286
    j = 166
    k = 144
    while True:
        triangle_num = i * (i + 1) / 2
        pentagonal_num = j * (3 * j - 1) / 2
        hexagonal_num = k * (2 * k - 1)
        minimum = min(triangle_num, pentagonal_num, hexagonal_num)
        if minimum == max(triangle_num, pentagonal_num, hexagonal_num):
            return triangle_num
        if minimum == triangle_num:
            i += 1
        if minimum == pentagonal_num:
            j += 1
        if minimum == hexagonal_num:
            k += 1

if __name__ == '__main__':
    print(compute())
