#
# Solution to Project Euler problem 30 - Digit fifth power
#
# https://gitlab.com/robbandersson/project_euler
#


def get_digit(num, n):
    return num // 10 ** n % 10


def fifth_power_sum(num):
    tot = 0
    for i in range(len(str(num))):
        tot += get_digit(num, i) ** 5
    return tot


def compute():
    tot = 0
    for i in range(10, 1000000):
        if i == fifth_power_sum(i):
            tot += i
    return tot


if __name__ == '__main__':
    print(compute())
