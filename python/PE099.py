#
# Solution to Project Euler problem 99 - Large exponential
#
# https://gitlab.com/robbandersson/project_euler
#
import math
import os
file_path = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))


def compute():
    with open(data_dir + '\p099_base_exp.txt') as f:
        in_file = f.read().splitlines()

    largest = 0
    idx = 0
    row_w_largest = 0
    for val in in_file:
        val = val.replace('\'', '').split(',')
        base = int(val[0])
        exp = int(val[1])
        print(idx, base, exp)
        if math.log(base, 10) * exp > largest:
            largest = math.log(base, 10) * exp
            row_w_largest = idx + 1
        idx += 1

    return row_w_largest


if __name__ == '__main__':
    print(compute())
