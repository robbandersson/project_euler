#
# Solution to Project Euler problem 12 - Highly divisible triangular number
#
# https://gitlab.com/robbandersson/project_euler
#
import sys, os
src_dir = os.path.normpath("..\src")
sys.path.append(src_dir)
from functions import num_divisors


def compute():
    curr_max = 1
    count = 1
    triangle = 1
    while True:
        if len(num_divisors(triangle)) > 500:
            break
        else:
            count += 1
            triangle += count
        if len(num_divisors(triangle)) > curr_max:
            curr_max = len(num_divisors(triangle))

    return triangle


if __name__ == '__main__':
    print(compute())
