#
# Solution to Project Euler problem 90 - Cube digit pairs
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools as it


def compute():
    pos_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    num_sides = 6 #only need to assign five of the sides for each die

    die_one = list(it.combinations(pos_num, num_sides))
    die_two = list(it.combinations(pos_num, num_sides))

    tuples_of_squares = [(0, 1), (0, 4), (0, 9), (1, 6), (2, 5), (3, 6), (4, 9), (6, 4), (8, 1)]

    count = 0
    for i in range(len(die_one)):
        # see if any of the dices contain one of 6 or 9. In that case, as the other as possible ints that can be produced
        if 9 in die_one[i] and 6 not in die_one[i]:
            die_one[i] = (6,) + die_one[i]
        elif 6 in die_one[i] and 9 not in die_one[i]:
            die_one[i] = (9,) + die_one[i]

        for j in range(len(die_two)):
            #see if any of the dices contain one of 6 or 9. In that case, as the other as possible ints that can be produced
            if 9 in die_two[j] and 6 not in die_two[j]:
                die_two[j] = (6,) + die_two[j]
            elif 6 in die_two[j] and 9 not in die_two[j]:
                die_two[j] = (9,) + die_two[j]

            for k in range(len(tuples_of_squares)):
                if (tuples_of_squares[k][0] in die_one[i] and tuples_of_squares[k][1] in die_two[j]) or \
                        (tuples_of_squares[k][1] in die_one[i] and tuples_of_squares[k][0] in die_two[j]):
                    flag = 0
                else:
                    flag = 1
                    break
            if flag == 0:
                count += 1

    #half the number of combinations since die one combination A and die two combination B is the same as die one combination B and die one combination A
    print(count/2)


if __name__ == '__main__':
    print(compute())
