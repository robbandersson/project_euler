#
# Solution to Project Euler problem 79 - Passcode derivation
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import re
file_path= os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))

def load_login(name):
    with open(data_dir + name) as f:
        attempt = f.read().splitlines()
    return attempt


def find_next(login_attempts, candidate):
    for attempt in login_attempts:
        if candidate in attempt[1:]:
            return find_next(login_attempts, attempt[0])
    return candidate


def compute():
    login_attempts = load_login('\p079_keylog.txt')

    passcode = list()
    while login_attempts:
        next_num = find_next(login_attempts, login_attempts[0][0])
        passcode.append(next_num)
        pop_idx = list()
        for i in range(len(login_attempts)):
            if next_num in login_attempts[i]:
                login_attempts[i] = re.sub(next_num, '', login_attempts[i])
                if login_attempts[i] == '':
                    pop_idx.append(i)
        for i in range(len(pop_idx)):
            login_attempts.pop(pop_idx[i] - i)

    return passcode


if __name__ == '__main__':
    print(compute())
