#
# Solution to Project Euler problem 38 - Pandigital multiples
#
# https://gitlab.com/robbandersson/project_euler
#


def is_pandigital(num_str, ref_str):
    if sorted(num_str) == sorted(ref_str):
        return True
    else:
        return False


def compute():
    ref_str = '123456789'
    max_pandigital = 0
    for num in range(100000):
        mult = 1
        num_str = ''
        while len(num_str) < 9:
            num_str += str(num * mult)
            mult += 1

        if is_pandigital(num_str, ref_str) and int(num_str) > max_pandigital:
            max_pandigital = int(num_str)

    return max_pandigital


if __name__ == '__main__':
    print(compute())
