#
# Solution to Project Euler problem 65 - Convergents of e
#
# https://gitlab.com/robbandersson/project_euler
#

def compute():
    upper = 101
    k = 1
    n = 2
    a0 = 2

    num1 = 1
    num2 = a0

    den1 = 0
    den2 = 1

    while n < upper:
        if n % 3 == 0:
            a = 2 * k
            k = k + 1
        else:
            a = 1

        num0 = int(num1)
        num1 = int(num2)
        num2 = int(a * num1 + num0)

        den0 = int(den1)
        den1 = int(den2)
        den2 = int(a * den1 + den0)

        n += 1

    tot = 0
    for i in range(len(str(num2))):
        tot += int(str(num2)[i])
    return tot

if __name__ == '__main__':
    print(compute())