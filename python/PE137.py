#
# Solution to Project Euler problem 137 - Fibonacci golden nuggets
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    fib_last = 1
    fib_curr = 2
    fib_next = 3

    n = 1
    while n < 15:
        print(n, fib_last * fib_curr)

        fib_last = fib_next
        fib_curr = fib_curr + fib_next
        fib_next = fib_curr + fib_next

        n += 1

    return n, fib_last * fib_curr

if __name__ == '__main__':
    print(compute())
