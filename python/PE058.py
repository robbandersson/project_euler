#
# Solution to Project Euler problem 58 - Spiral primes
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime


def compute():
    square_num = 3
    diagonal = 5
    prime_diagonal = 3
    prime_frac = prime_diagonal / diagonal
    while prime_frac > 0.1:
        square_num += 2
        for k in range(1, 4):
            if is_prime(square_num ** 2 - k * (square_num - 1)):
                prime_diagonal += 1
        diagonal += 4
        prime_frac = prime_diagonal / diagonal
    return square_num


if __name__ == '__main__':
    print(compute())
