#
# Solution to Project Euler problem 76 - Counting summations
#
# https://gitlab.com/robbandersson/project_euler
#
# The number of unique ways to sum the number n, S(n), relates to the number of partitions of n, p(n), in that
# S(n = p(n) - 1 (as the single partitioning n is disregarded for the sum). Furthermore, the number of partitions can
# be calculated using Euler's pentagonal number theorem. Thus, by calculating the number of partitions and remove one,
# we will arrive at the correct answer.


def first_pentagonal(n):
    return int(n * (3 * n - 1) / 2)


def second_pentagonal(n):
    return int(n * (3 * n + 1) / 2)


def compute():
    limit = 100
    partitions = [1, 1, 2]
    while len(partitions) < limit + 1:
        n = len(partitions) + 1
        pn = 0
        k = 1
        while n - first_pentagonal(k) > 0:
            pn += (-1) ** (k - 1) * partitions[n - first_pentagonal(k) - 1]
            k += 1

        k = 1
        while n - second_pentagonal(k) > 0:
            pn += (-1) ** (k - 1) * partitions[n - second_pentagonal(k) - 1]
            k += 1

        partitions.append(pn)

    return partitions[-1] - 1


if __name__ == '__main__':
    print(compute())
