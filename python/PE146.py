#
# Solution to Project Euler problem 146 - Investigating a Prime Patterns
#
# https://gitlab.com/robbandersson/project_euler
#
import time
import random
from src.functions import is_prime, prime_factorize


start_time = time.time()


def miller_rabin(n, k):

    if n == 2:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


def create_modulus_dict(upper, factors, prime_add):
    modulus = dict()
    n = 7
    while n < upper:
        if is_prime(n):
            if n in factors:
                i = 1
            else:
                i = 0
            mod = set()
            while i < n:
                for j in [x % n for x in prime_add]:
                    if (j + i ** 2) % n == 0:
                        mod.add(i)
                i += 1
            modulus[n] = list(mod)
        n += 2

    return modulus


def compute():
    miller_rabin_const = 40
    tot = 0
    factors = [3, 7, 13]
    prime_add = [1, 3, 7, 9, 13, 27]
    upper = 30
    modulus = create_modulus_dict(upper, factors, prime_add)

    for n in range(10, 150000000, 10):
        divisible_flag = False
        for p in factors:
            if n % p == 0:
                divisible_flag = True
                break

        if not divisible_flag:
            modulus_flag = True
            for key, values in modulus.items():
                for val in values:
                    if n > key:
                        if n % key == val:
                            modulus_flag = False
                            break
                if not modulus_flag:
                    break
            if modulus_flag:
                if not miller_rabin(n ** 2 + 19, miller_rabin_const) and not miller_rabin(n ** 2 + 21, miller_rabin_const):
                    prime_flag = True
                    for m in prime_add:
                        if not miller_rabin(n ** 2 + m, miller_rabin_const):
                            prime_flag = False
                            break
                    if prime_flag:
                        tot += n
                        print(prime_factorize(n), n, tot)

    print('Elapsed time', round(time.time()-start_time, 2), 'seconds.')
    return tot


if __name__ == '__main__':
    print(compute())
