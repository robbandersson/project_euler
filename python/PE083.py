#
# Solution to Project Euler problem 83 - Path sum: four ways
#
# https://gitlab.com/robbandersson/project_euler
#
# Solved using Dijkstra's algorithm
import os
import numpy as np
data_dir = os.path.normpath("..\data")


def load_matrix(name):
    with open(data_dir + name) as f:
        lines = [list(map(int, line.split(','))) for line in f]
    return lines


def find_neighbours(node, n):
    (x, y) = node
    if x == 0:
        if y == 0:
            neighbours = [(x, y + 1), (x + 1, y)]
        elif y == n:
            neighbours = [(x + 1, y), (x, y - 1)]
        else:
            neighbours = [(x, y - 1), (x, y + 1), (x + 1, y)]
    elif x == n:
        if y == 0:
            neighbours = [(x, y + 1), (x - 1, y)]
        elif y == n:
            neighbours = [(x - 1, y), (x, y - 1)]
        else:
            neighbours = [(x, y - 1), (x, y + 1), (x - 1, y)]
    else:
        if y == 0:
            neighbours = [(x, y + 1), (x + 1, y), (x - 1, y)]
        elif y == n:
            neighbours = [(x, y - 1), (x + 1, y), (x - 1, y)]
        else:
            neighbours = [(x, y + 1), (x, y - 1), (x + 1, y), (x - 1, y)]
    return neighbours


def compute():
    matrix = load_matrix('\p083_matrix.txt')
    n = len(matrix)
    path_mat = np.full((n, n), np.inf)
    path_mat[0, 0] = matrix[0][0]

    visited = set()
    unvisited = set()
    for i in range(n):
        for j in range(n):
            unvisited.add((i, j))

    current = (0, 0)
    while (n - 1, n - 1) not in visited:
        (x0, y0) = current
        # Find all unvisited neighbours to the current node
        neighbours = find_neighbours(current, n - 1)
        for (x, y) in neighbours:
            if (x, y) in unvisited:
                # Compare assigned value to current value
                if path_mat[x, y] > path_mat[x0, y0] + matrix[x][y]:
                    path_mat[x, y] = path_mat[x0, y0] + matrix[x][y]

        # Mark current node as visited
        visited.add(current)
        # Remove current node from unvisited
        unvisited.remove(current)
        # Find next current node
        current_value = np.inf
        for (x, y) in unvisited:
            if path_mat[x, y] < current_value:
                current_value = path_mat[x, y]
                current = (x, y)

    return path_mat[n - 1, n - 1]


if __name__ == '__main__':
    print(compute())
