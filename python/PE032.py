#
# Solution to Project Euler problem 32 - Pandigital products
#
# https://gitlab.com/robbandersson/project_euler
#
import numpy as np


def factors(n):
    fac_arr = np.array([])
    for i in range(1, n + 1):
        if n % i == 0:
            fac_arr = np.append(fac_arr, i)
    return fac_arr


def get_digit(num, n):
    return num // 10**n % 10


def compute():
    pandig_set = set()
    for i in range(1000, 10001):
        fac_arr = factors(i)
        for j in range(len(fac_arr)):
            all_digits = list()

            mult = int(fac_arr[j])
            for k in range(len(str(mult))):
                all_digits.append(get_digit(mult, k))

            multicand = int(i / fac_arr[j])
            for k in range(len(str(multicand))):
                all_digits.append(get_digit(multicand, k))

            # print(mult, multicand)
            for k in range(len(str(i))):
                all_digits.append(get_digit(i, k))

            if sorted(all_digits) == list(range(1, 10)):
                pandig_set.add(i)
                # print(i, mult, multicand)
    tot = 0
    for val in pandig_set:
        tot += val
    return tot


if __name__ == '__main__':
    print(compute())
