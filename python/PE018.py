#
# Solution to Project Euler problem 18 - Maximum path sum I
#
# https://gitlab.com/robbandersson/project_euler
#
import numpy as np
import os
data_dir = os.path.normpath("..\data")


def compute():
    arr = [[int(i) for i in line.split()] for line in open('\PE18.txt')]

    max_route_sum = 0
    for j in range(10000):
        route_sum = arr[0][0]
        pos = 0
        for i in range(1, len(arr)):
            row_sum = arr[i][pos] + arr[i][pos + 1]
            rand = np.random.uniform(0, row_sum)
            if rand < arr[i][pos]:
                route_sum += arr[i][pos]
            else:
                route_sum += arr[i][pos + 1]
                pos += 1
        if route_sum > max_route_sum:
            max_route_sum = route_sum
    return max_route_sum


if __name__ == '__main__':
    print(compute())
