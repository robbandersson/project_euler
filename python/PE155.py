#
# Solution to Project Euler problem 155 - Counting capacitor circuits
#
# https://gitlab.com/robbandersson/project_euler
#
from fractions import Fraction

capacitor = 60
capacitance = dict()
capacitance[1] = {Fraction(1, capacitor)}


def capacity(n):
    if n in capacitance:
        return capacitance[n]
    ans = set()
    for a in range(1, n):
        for x in capacity(a):
            for y in capacity(n - a):
                ans.add(x + y)
                ans.add(1 / (1 / x + 1 / y))
    capacitance[n] = ans
    return capacity(n)


def compute():
    SIZE = 18
    aggregate = set()
    for i in range(1, SIZE + 1):
        aggregate.update(capacity(i))
        print(i)
    return len(aggregate)


if __name__ == '__main__':
    print(compute())

