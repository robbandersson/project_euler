#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import euler_totient


def compute():
    upper = 10000000
    n = 2
    min_ratio = 10000000
    while n < upper:
        euler_t = euler_totient(n)
        if sorted(str(euler_t)) == sorted(str(n)):
            ratio = n / euler_t
            if ratio < min_ratio:
                min_ratio = ratio
                result = n
        n += 1

    return result


if __name__ == '__main__':
    print(compute())

