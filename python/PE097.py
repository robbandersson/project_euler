#
# Solution to Project Euler problem 97 - Large non-Mersenne prime
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    exponent = 7830457
    num = 1
    count = 0
    while count < exponent:
        num *= 2
        if len(str(num)) > 10:
            num = int(str(num)[-10:])
        count += 1
    num = num * 28433
    num += 1
    return int(str(num)[-10:])

if __name__ == '__main__':
    print(compute())
