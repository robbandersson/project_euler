#
# Solution to Project Euler problem 85 - Counting rectangles
#
# https://gitlab.com/robbandersson/project_euler
#
# 2000 x 1 yields 2001000 rectangles. We don't need to search beyond the size of 2000 for x and y. That standard case
# yields an area of 2000
import numpy as np


def compute():
    target = 2000000
    error = 1000
    x_max = 2000
    y_max = 2000
    area = 2000
    for x in range(1, x_max):
        for y in range(1, y_max):
            num_rectangles = x * (x + 1) * y * (y + 1) / 4
            if np.abs(num_rectangles - target) < error:
                error = np.abs(num_rectangles - target)
                area = x * y
    return area


if __name__ == '__main__':
    print(compute())
