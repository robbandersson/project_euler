#
# Solution to Project Euler problem 91 - Right triangles with integer coordinates
#
# https://gitlab.com/robbandersson/project_euler
#


def is_right_triangle(P, Q):
    a = P[0] ** 2 + P[1] ** 2
    b = Q[0] ** 2 + Q[1] ** 2
    c = (P[0] - Q[0]) ** 2 + (P[1] - Q[1]) ** 2
    return (a + b == c) or (a + c == b) or (b + c == a)

def compute():
    upper = 50
    count = 0
    for x1 in range(0, upper + 1):
        for x2 in range(upper + 1):
            for y1 in range(upper + 1):
                for y2 in range(upper + 1):
                    P = (x1, y1)
                    Q = (x2, y2)
                    if is_right_triangle(P, Q) and y1 * x2 < y2 * x1:
                        count += 1
    return count


if __name__ == '__main__':
    print(compute())
