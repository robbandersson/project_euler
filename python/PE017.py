#
# Solution to Project Euler problem 17 - Number letter counts
#
# https://gitlab.com/robbandersson/project_euler
#
import inflect


def compute():
    p = inflect.engine()
    tot = 0
    for i in range(1, 1001):
        num_as_str = p.number_to_words(i)
        num_as_str = num_as_str.replace(' ', '')
        num_as_str = num_as_str.replace('-', '')
        tot += len(num_as_str)
    return tot


if __name__ == '__main__':
    print(compute())
