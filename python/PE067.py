#
# Solution to Project Euler problem 67 - Maximum path sum II
#
# https://gitlab.com/robbandersson/project_euler
#
import os
data_dir = os.path.normpath("data")


def dijkstra(arr):
    arr[1][0] = arr[0][0] + arr[1][0]
    arr[1][1] = arr[0][0] + arr[1][1]
    for i in range(2, len(arr)):
        # update the outer positions in the triangle
        arr[i][0] = arr[i][0] + arr[i-1][0]
        arr[i][len(arr[i])-1] = arr[i][len(arr[i])-1] + arr[i - 1][len(arr[i - 1]) - 1]
        for j in range(1, len(arr[i]) - 1):
            arr[i][j] = arr[i][j] + max(arr[i-1][j-1], arr[i-1][j])
    return arr


def compute():
    arr = [[int(i) for i in line.split()] for line in open(data_dir + '\p067_triangle.txt')]
    route_arr=dijkstra(arr)
    return max(route_arr[len(route_arr) - 1])


if __name__ == '__main__':
    print(compute())