#
# Solution to Project Euler problem 152 - Sums of Squares Reciprocals
#
# https://gitlab.com/robbandersson/project_euler
#
import os, sys
import itertools as it
from math import gcd
from fractions import Fraction
import time
sys.path.append(os.path.join(os.path.dirname(__file__), "../src"))
from functions import prime_factorize, is_prime

def get_previous_prime(prime):
    if prime == 3:
        return 2

    p = prime - 2
    while not is_prime(p):
        p -= 2
    return p


def find_squared_reciprocal_sum(candidates, candidates_tot, series, tot):
    global target
    global num_found
    if tot == target:
        num_found += 1
        return

    global squared_reciprocal_sums
    if not candidates:
        if target - tot in squared_reciprocal_sums:
            num_found += squared_reciprocal_sums[target - tot]
        return

    next_candidates = candidates.copy()
    next_candidates_tot = candidates_tot
    next_series = series.copy()
    next_tot = tot

    global max_remaining
    if tot + candidates_tot + max_remaining >= target:
        next_candidates.remove(candidates[0])
        next_candidates_tot -= Fraction(1, candidates[0] ** 2)
        if tot + Fraction(1, candidates[0] ** 2) > target:
            find_squared_reciprocal_sum(next_candidates, next_candidates_tot, next_series, next_tot)
        elif tot + next_candidates_tot + max_remaining < target:
            next_series.append(candidates[0])
            next_tot += Fraction(1, candidates[0] ** 2)
            find_squared_reciprocal_sum(next_candidates, next_candidates_tot, next_series, next_tot)
        else:
            find_squared_reciprocal_sum(next_candidates, next_candidates_tot, next_series, next_tot)

            next_series.append(candidates[0])
            next_tot += Fraction(1, candidates[0] ** 2)
            find_squared_reciprocal_sum(next_candidates, next_candidates_tot, next_series, next_tot)

def compute():
    start = time.time()
    upper = 80
    global target
    target = Fraction(1, 2)
    candidates = list(range(2, upper + 1))
    factorization = {}
    max_prime_factor = 2
    for k in candidates:
        factorization[k] = prime_factorize(k)
        m = max(factorization[k])
        if m > max_prime_factor:
            max_prime_factor = m

    flag = True
    while flag:
        relative_prime_candidates = []
        for k in candidates:
            if k % max_prime_factor == 0:
                relative_prime_candidates.append(k)
        if len(relative_prime_candidates) == 1:
            candidates.remove(relative_prime_candidates[0])
            factorization.pop(relative_prime_candidates[0])
        else:
            found_combination = False
            possible_coprimes = set()
            for l in range(2, len(relative_prime_candidates) + 1):
                for comb in it.combinations(relative_prime_candidates, l):
                    lcm = 1
                    for val in comb:
                        lcm = lcm * val // gcd(val, lcm)

                    num = 0
                    for val in comb:
                        num += (lcm // val) ** 2

                    if num % max_prime_factor ** 2 == 0:
                        found_combination = True
                        possible_coprimes = possible_coprimes.union(set(comb))
            if not found_combination: 
                for k in relative_prime_candidates:
                    candidates.remove(k)
                    factorization.pop(k)
            else:
                for elem in relative_prime_candidates:
                    if elem not in possible_coprimes:
                        candidates.remove(elem)
                        factorization.pop(elem)
        max_prime_factor = get_previous_prime(max_prime_factor)
        if max_prime_factor == 3:
            flag = False

    global squared_reciprocal_sums
    squared_reciprocal_sums = dict()
    div_num = upper // 2
    upper_factors = [val for val in candidates if val > div_num]
    for l in range(1, len(upper_factors) + 1):
        for comb in it.combinations(upper_factors, l):
            tot = Fraction(0, 1)
            for val in comb:
                tot += Fraction(1, val ** 2)

            if tot in squared_reciprocal_sums:
                squared_reciprocal_sums[tot] += 1
            else:
                squared_reciprocal_sums[tot] = 1
    global max_remaining
    max_remaining = tot

    candidates = [val for val in candidates if val not in upper_factors]
    candidates_tot = Fraction(0, 1)
    for val in candidates:
        candidates_tot += Fraction(1, val ** 2)
    global num_found
    num_found = 0
    find_squared_reciprocal_sum(candidates, candidates_tot, [], Fraction(0, 1))

    end = time.time()
    print("Time consumed in working:",round(end - start, 3), "seconds")
    return num_found

if __name__ == '__main__':
    print(compute())
