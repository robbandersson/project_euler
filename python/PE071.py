#
# Solution to Project Euler problem 71 - Ordered fractions
#
# https://gitlab.com/robbandersson/project_euler
#
import fractions
import math


def compute():
    target = fractions.Fraction(3, 7)
    closest = fractions.Fraction(2, 5)
    d = 9
    upper = 1000000
    while d <= upper:
        n = math.floor(d * target.numerator / target.denominator)
        if n < d:
            frac = fractions.Fraction(n, d)

            if closest < frac < target:
                closest = frac
        d += 1

    return closest.numerator


if __name__ == '__main__':
    print(compute())
