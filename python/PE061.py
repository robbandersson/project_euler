#
# Solution to Project Euler problem 61 - Cyclical figurate numbers
#
# https://gitlab.com/robbandersson/project_euler
#
import random


def get_polygonal_num(lower, upper):
    triangle_num = 1
    n = 1
    tri = list()
    sqr = list()
    penta = list()
    hexa = list()
    hepta = list()
    octa = list()
    while triangle_num < upper:
        triangle_num = int(n * (n + 1) / 2)
        if lower < triangle_num < upper and str(triangle_num)[-2] != '0':
            tri.append(triangle_num)

        square_num = n ** 2
        if lower < square_num < upper and str(square_num)[-2] != '0':
            sqr.append(square_num)

        pentagonal_num = int(n * (3 * n - 1) / 2)
        if lower < pentagonal_num < upper and str(pentagonal_num)[-2] != '0':
            penta.append(pentagonal_num)

        hexagonal_num = n * (2 * n - 1)
        if lower < hexagonal_num < upper and str(hexagonal_num)[-2] != '0':
            hexa.append(hexagonal_num)

        heptagonal_num = int(n * (5 * n - 3) / 2)
        if lower < heptagonal_num < upper and str(heptagonal_num)[-2] != '0':
            hepta.append(heptagonal_num)

        octagonal_num = n * (3 * n - 2)
        if lower < octagonal_num < upper and str(octagonal_num)[-2] != '0':
            octa.append(octagonal_num)

        n += 1
    return tri, sqr, penta, hexa, hepta, octa


def find_candidates(num_list, prefix):
    candidates = list()
    for val in num_list:
        if str(val)[0:2] == prefix:
            candidates.append(val)
    return candidates


def compute():
    triangle, square, pentagonal, hexagonal, heptagonal, octagonal = get_polygonal_num(999, 10000)
    seq_order = ['tri', 'square', 'penta', 'hexa', 'hepta']

    found = False
    while not found:
        seed = random.sample(octagonal, 1)[0]
        seq = [seed]
        next_prefix = str(seed)[-2:]
        random.shuffle(seq_order)

        for k in range(len(seq_order)):
            if seq_order[k] == 'tri':
                candidates = find_candidates(triangle, next_prefix)
            elif seq_order[k] == 'square':
                candidates = find_candidates(square, next_prefix)
            elif seq_order[k] == 'penta':
                candidates = find_candidates(pentagonal, next_prefix)
            elif seq_order[k] == 'hexa':
                candidates = find_candidates(hexagonal, next_prefix)
            else:
                candidates = find_candidates(heptagonal, next_prefix)
            if candidates:
                next_val = random.sample(candidates, 1)[0]
                next_prefix = str(next_val)[-2:]
                seq.append(next_val)
            else:
                break

        if len(seq) == 6 and str(seed)[0:2] == next_prefix:
            return sum(seq)


if __name__ == '__main__':
    print(compute())
