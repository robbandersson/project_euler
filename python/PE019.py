#
# Solution to Project Euler problem 19 - Counting Sundays
#
# https://gitlab.com/robbandersson/project_euler
#


def first_sun(year, start_day):
    first_day_list = [0] * 12
    month_list = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if year % 4 == 0:
        if year % 100 == 0 and year % 400 != 0:
            month_list[1] = 28
        else:
            month_list[1] = 29
    day = start_day
    for i in range(len(month_list)):
        first_day_list[i] = day
        for j in range(month_list[i]):
            day = (day + 1) % 7
    end_day = day
    num_sun = first_day_list.count(6)
    return end_day, num_sun


def compute():
    start_day = 1
    tot = 0
    for year in range(1901, 2001):
        (end_day, num_sun) = first_sun(year, start_day)
        tot += num_sun
        start_day = end_day

    return tot


if __name__ == '__main__':
    print(compute())
