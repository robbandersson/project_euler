#
# Solution to Project Euler problem 21 - Amicable numbers
#
# https://gitlab.com/robbandersson/project_euler
#
import itertools as it
from itertools import accumulate
from operator import mul


def prod(lst):
    for value in accumulate(lst, mul):
        pass
    return value


def find_prim_factors(num):
    i = 2
    factors = []
    while num % i == 0:
        num = num / i
        factors.append(i)
    i = 3
    while i * i <= num:
        while num % i == 0:
            num = num / i
            factors.append(i)
        i += 2
    if num > 1:
        factors.append(int(num))
    return factors


def power_set(l):
    pow_set = set()
    for i in range(1, len(l) + 1):
        for sub_sets in it.combinations(l, i):
            pow_set.add(sub_sets)
    return list(pow_set)


def find_proper_divisors(num):
    if num == 0:
        return [0]
    else:
        divisor_list = [1]
        prime_factors = find_prim_factors(num)
        prime_permute = power_set(prime_factors)
        for i in range(len(prime_permute)):
            divisor_list.append(prod(prime_permute[i]))
        divisor_list.remove(num)
        return divisor_list


def compute():
    max_num = 10000
    sum_list = [0] * max_num
    amicable_num = []
    for i in range(len(sum_list)):
        sum_list[i] = sum(find_proper_divisors(i+1))
        if i + 1 != sum_list[i] and sum(find_proper_divisors(sum_list[i])) == i + 1:
            amicable_num.append(i + 1)

    return sum(amicable_num)


if __name__ == '__main__':
    print(compute())
