#
# Solution to Project Euler problem 25 - 1000-digit Fibonacci number
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    Fibonacci = [1, 1]
    n = 1
    while len(str(Fibonacci[n])) < 1000:
        Fibonacci.append(Fibonacci[n] + Fibonacci[n-1])
        n += 1

    return n + 1


if __name__ == '__main__':
    print(compute())
