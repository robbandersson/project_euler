#
# Solution to Project Euler problem 69 - Totient maximum
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import prime_factorize


def compute():
    upper = 1000000
    n = 2
    max_quote = 0
    retult = 0
    while n < upper:
        prime_factors = prime_factorize(n)
        euler_totient = n
        for p in set(prime_factors):
            euler_totient *= (1 - 1 / p)

        if n / euler_totient > max_quote:
            max_quote = n / euler_totient
            result = n
        n += 1

    return result


if __name__ == '__main__':
    print(compute())

