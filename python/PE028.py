#
# Solution to Project Euler problem 28 - Number spiral diagonals
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    tot = 1
    for i in range(3, 1002, 2):
        for j in range(4):
            tot += i ** 2 - (i - 1) * j
    return tot


if __name__ == '__main__':
    print(compute())
