#
# Solution to Project Euler problem 4 - Largest palindrome product
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_pal


def compute():
    largest_pal = 0
    for i in range(100, 1000):
        for j in range(100, 1000):
            if is_pal(i * j) and i * j > largest_pal:
                largest_pal = i * j
    return largest_pal


if __name__ == '__main__':
    print(compute())
