#
# Solution to Project Euler problem 140 - Modified Fibonacci golden nuggets
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    seeds = [(2, -7),
             (0, -1),
             (0, 1),
             (-4, 5),
             (-3, 2),
             (-3, -2)]

    nuggets = list()
    for seed in seeds:
        k = seed[0]
        x = seed[1]

        for i in range(30):
            k_next = -9 * k + -4 * x + -14
            x_next = -20 * k + -9 * x + -28

            k = k_next
            x = x_next
            if k > 0:
                nuggets.append(k)

    nuggets = sorted(nuggets)
    tot = 0
    for i in range(30):
        tot += nuggets[i]
    return tot


if __name__ == '__main__':
    print(compute())