#
# Solution to Project Euler problem 59 - XOR decryption
#
# https://gitlab.com/robbandersson/project_euler
#
import os
data_dir = os.path.normpath("data")


def load_crypto(name):
    with open(data_dir + name) as f:
        cipher = f.read().strip().split(',')
    return list(map(int, cipher))


def decrypt(cipher, key):
    return ''.join(chr(a ^ b) for a, b in zip(cipher, key))


def is_encrypted(text):
    if all (x in text for x in [' the ', ' and ']):
        return False
    else:
        return True


def update_key(key, english_letters):
    if key[0] != english_letters[-1]:
        key[0] += 1
    elif key[1] != english_letters[-1]:
        key[0] = english_letters[0]
        key[1] += 1
    else:
        key[0] = english_letters[0]
        key[1] = english_letters[0]
        key[2] += 1
    return key


def to_ascii(text):
    ascii_list = list()
    for letter in text:
        ascii_list.append(ord(letter))
    return ascii_list


def compute():
    cipher = load_crypto('\p059_cipher.txt')
    english_letters = list(range(97, 123))
    encrypted = True
    key = [english_letters[0]] * 3
    while encrypted:
        cyclic_key = key * (len(cipher) // len(key))
        text = decrypt(cipher, cyclic_key)

        if is_encrypted(text):
            key = update_key(key, english_letters)
        else:
            print(text)
            return sum(to_ascii(text))


if __name__ == '__main__':
   print(compute())
