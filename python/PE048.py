#
# Solution to Project Euler problem 48 - Self powers
#
# https://gitlab.com/robbandersson/project_euler
#


def compute():
    tot = 0
    ref_num = 9999999999
    for num in range(1, 1001):
        if tot < ref_num:
            tot += num ** num
        else:
            trim_prod = num
            for k in range(2, num + 1):
                trim_prod = trim_prod * num
                if trim_prod > ref_num:
                    trim_prod = int(str(trim_prod)[-10:])
            tot += trim_prod
    return int(str(tot)[-10:])


if __name__ == '__main__':
    print(compute())
