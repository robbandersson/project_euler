#
# Solution to Project Euler problem 36 - Double-base palindromes
#
# https://gitlab.com/robbandersson/project_euler
#


def is_palindrome(s):
    return s == s[::-1]


def compute():
    get_bin = lambda x: format(x, 'b')
    tot = 0
    for num in range(1000000):
        if is_palindrome(str(num)):
            if is_palindrome(str(get_bin(num))):
                tot += num

    return tot


if __name__ == '__main__':
    print(compute())
