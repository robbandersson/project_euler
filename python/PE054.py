#
# Solution to Project Euler problem 54 - Poker hands
#
# https://gitlab.com/robbandersson/project_euler
#
import os
data_dir = os.path.normpath("data")


def hand_value(hand):
    hand = hand.split()
    hand_suit = list()
    hand_val = list()
    # Replace J - 11, Q - 12, K - 13, A - 14
    for card in hand:
        hand_suit.append(card[1])
        hand_val.append(card[0].replace('T', '10').replace('J', '11').replace('Q', '12').replace('K', '13').replace('A', '14'))
    hand_val = list(map(int, hand_val))

    if len(set(hand_suit)) == 1:
        FLUSH = True
    else:
        FLUSH = False

    if sorted(hand_val) == list(range(min(hand_val), min(hand_val) + 5)):
        STRAIGHT = True
        high_val = max(hand_val)
    elif sorted(hand_val) == list(range(min(hand_val), min(hand_val) + 4)).append(14): # Ace-low straight
        STRAIGHT = True
        high_val = 4
    else:
        STRAIGHT = False

    if FLUSH == True and STRAIGHT == True:
        if min(hand_val) == 10:
            return [9]
        else:
            return [8] + [max(hand_val)]
    elif FLUSH == True:
        return [5] + sorted(hand_val)[::-1]
    elif STRAIGHT == True:
        return [4] + [high_val]

    card_dict = {i: hand_val.count(i) for i in hand_val}
    if len(card_dict) == 2: # Full house or four of a kind
        value = [0, 0, 0]
        for key, val in card_dict.items():
            if val == 1:
                value[2] = key
            elif val == 2:
                value[2] = key
            elif val == 3:
                value[0] = 6
                value[1] = key
            else:
                value[0] = 7
                value[1] = key
        return value
    elif len(card_dict) == 3: # Three of a kind or two-pair
        value = [0, 0, 0, 0]
        if 3 in card_dict.values(): # Three of a kind
            stored_key = 0
            value[0] = 3
            for key, val in card_dict.items():
                if val == 1:
                    if stored_key == 0:
                        stored_key = key
                    elif key > stored_key:
                        value[2] = key
                        value[3] = stored_key
                    else:
                        value[2] = stored_key
                        value[3] = key
                else:
                    value[1] = key
        else:
            stored_pair = 0
            value[0] = 2
            for key, val in card_dict.items():
                if val == 1:
                    value[3] = key
                else:
                    if stored_pair == 0:
                        stored_pair = key
                    elif key > stored_pair:
                        value[1] = key
                        value[2] = stored_pair
                    else:
                        value[1] = stored_pair
                        value[2] = key
        return value
    elif len(card_dict) == 4: # Pair
        value = [0, 0]
        stored_keys = [0, 0, 0]
        for key, val in card_dict.items():
            if val == 2:
                value[0] = 1
                value[1] = key
            else:
                if stored_keys[0] == 0:
                    stored_keys[0] = key
                elif stored_keys[0] == 0:
                    stored_keys[1] = key
                else:
                    stored_keys[2] = key
                    stored_keys = sorted(stored_keys)[::-1]
        return value + stored_keys
    else: # High card
        return [0] + sorted(hand_val)[::-1]

def compute():
    win_p1 = 0
    win_p2 = 0
    num_hands = 0
    with open(data_dir + '/p054_poker.txt', 'r') as f:
        for line in f:
            current_line = line.split(",")
            for hands in current_line:
                p1_hand = hands[:14]
                p2_hand = hands[15:]
                val_p1 = hand_value(p1_hand)
                val_p2 = hand_value(p2_hand)
                idx = 0
                while val_p1[idx] == val_p2[idx]:
                    idx += 1
                if val_p1[idx] > val_p2[idx]:
                    win_p1 += 1
                elif val_p1[idx] < val_p2[idx]:
                    win_p2 += 1
    return win_p1


if __name__ == '__main__':
    print(compute())