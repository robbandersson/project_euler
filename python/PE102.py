#
# Solution to Project Euler problem 102 - Triangle containment
#
# https://gitlab.com/robbandersson/project_euler
#
import os
import numpy as np
file_path = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(file_path) + os.path.normpath("\data"))


def compute():
    with open(data_dir + '\p102_triangles.txt') as f:
        all_points = f.read().replace("\n", ",").replace("\'", "").split(",")

    num_contains = 0
    for i in range((len(all_points) - 1) // 6):
        idx = i * 6
        v0 = np.array([int(all_points[idx]), int(all_points[idx + 1]), 0])
        v1 = np.array([int(all_points[idx + 2]), int(all_points[idx + 3]), 0])
        v2 = np.array([int(all_points[idx + 4]), int(all_points[idx + 5]), 0])

        a = np.cross(v1 - v0, -v0)
        b = np.cross(v2 - v1, -v1)
        c = np.cross(v0 - v2, -v2)

        if np.sign(a[2]) == np.sign(b[2]) and np.sign(b[2]) == np.sign(c[2]):
            num_contains += 1

    return num_contains


if __name__ == '__main__':
    print(compute())