#
# Solution to Project Euler problem 86 - Cuboid route
#
# https://gitlab.com/robbandersson/project_euler
#
import math

def combination(n, r):
    return int(math.factorial(n) / (math.factorial(n - r) * math.factorial(r)))

def pascal_triangle(rows):
    results = []
    for count in range(rows):
        row = []
        for element in range(count + 1):
            row.append(combination(count, element))
        results.append(row)
    return results

print(pascal_triangle(20))
