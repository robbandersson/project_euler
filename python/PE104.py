#
# Solution to Project Euler problem  - 
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_pandigital


def compute():
    f_last = 1
    f_curr = 1
    g_last = 1
    g_curr = 1
    k = 3
    found = False
    while not found:
        f_next = f_curr + f_last
        g_next = g_curr + g_last

        if len(str(f_next)) > 9:
            f_next = int(str(f_next)[-9:])

        if len(str(g_next)) > 40:
            g_next = int(str(g_next)[0:40])
            g_last = g_last // 10
            g_curr = g_curr // 10

        if (len(str(g_next)) > 9 and is_pandigital(int(str(g_next)[0:9])) and
                    len(str(f_next)) == 9 and is_pandigital(f_next)):
            found = True
            print(int(str(g_next)[0:10]))
        else:
            g_last = g_curr
            g_curr = g_next
            f_last = f_curr
            f_curr = f_next
            k += 1

    return k


if __name__ == '__main__':
    print(compute())
