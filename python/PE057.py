#
# Solution to Project Euler problem 57 - Square root covergents
#
# https://gitlab.com/robbandersson/project_euler
#
from fractions import Fraction
import sys
sys.setrecursionlimit(1500)


def fraction_recursive(frac, n):
    if n > 1:
        return fraction_recursive(Fraction(1, 2 + frac), n - 1)
    else:
        return frac


def compute():
    frac_source = Fraction(2, 5)
    tot = 0
    for n in range(1, 1000):
        inner_frac = fraction_recursive(frac_source, n)
        frac = Fraction(inner_frac.denominator + inner_frac.numerator, inner_frac.denominator)
        if len(str(frac.numerator)) > len(str(frac.denominator)):
            tot += 1
    return tot


if __name__ == '__main__':
    print(compute())
