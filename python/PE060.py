#
# Solution to Project Euler problem 60 - Prime pair set
#
# https://gitlab.com/robbandersson/project_euler
#
from src.functions import is_prime
from itertools import combinations


def is_prime_pair(p1, p2):
    return is_prime(int(str(p1) + str(p2))) and is_prime(int(str(p2) + str(p1)))


def compute():

    primes = [2]
    n = 3
    prime_pairs = dict()
    found = False
    threshold = 4
    while not found:
        if is_prime(n):
            candidates = set()
            for p in primes:
                if is_prime_pair(p, n):
                    if p in prime_pairs:
                        prime_pairs[p].add(n)
                    else:
                        prime_pairs[p] = {n}
                    if len(prime_pairs[p]) >= threshold - 1:
                        candidates.add(p)

                    if n in prime_pairs:
                        prime_pairs[n].add(p)
                    else:
                        prime_pairs[n] = {p}
                    if len(prime_pairs[n]) >= threshold - 1:
                        candidates.add(n)

            if len(candidates) >= threshold:
                for p1 in candidates:
                    for comb in combinations(prime_pairs[p1].intersection(candidates), threshold - 1):
                        prime_set = prime_pairs[p1].union({p1})
                        count = 0
                        for p2 in comb:
                            count += 1
                            prime_set = prime_set.intersection(prime_pairs[p2].union({p2}))
                        if len(prime_set) >= threshold and count >= threshold - 1:
                            return sum(prime_set)
            primes.append(n)
        n += 2


if __name__ == '__main__':
   print(compute())
