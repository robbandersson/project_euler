#
# Solution to Project Euler problem 84 - Monopoly odds
#
# https://gitlab.com/robbandersson/project_euler
#
import random


def compute():
    CC_cards = ['GO', 'JAIL', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0']
    CH_cards = ['GO', 'JAIL', 'C1', 'E3', 'H2', 'R1', 'NEXT_R', 'NEXT_R', 'NEXT_U', 'BACK', '0', '0', '0',
                '0', '0', '0']

    square_names = ['GO', 'A1', 'CC1', 'A2', 'T1', 'R1', 'B1', 'CH1', 'B2', 'B3', 'JAIL', 'C1', 'U1', 'C2', 'C3', 'R2',
               'D1', 'CC2', 'D2', 'D3', 'FP', 'E1', 'CH2', 'E2', 'E3', 'R3', 'F1', 'F2', 'U2', 'F3', 'G2J', 'G1',
               'G2', 'CC3', 'G3', 'R4', 'CH3', 'H1', 'T2', 'H2']
    square_pos = list(range(len(square_names)))
    square_pos_names = dict(zip(square_pos, square_names))
    square_names_pos = dict(zip(square_names, square_pos))

    square_counts = dict()
    for square in square_names:
        square_counts[square] = 0

    pos = 0
    CH_idx = 0
    CC_idx = 0
    random.shuffle(CC_cards)
    random.shuffle(CH_cards)

    for i in range(10000):
        d1 = random.randint(1, 4)
        d2 = random.randint(1, 4)

        pos += (d1 + d2)
        if pos >= len(square_names):
            pos -= len(square_names)

        if square_pos_names[pos] == 'G2J':
            pos = square_names_pos['JAIL']

        elif 'CH' in square_pos_names[pos]:
            if CH_cards[CH_idx] in square_names:
                pos = square_names_pos[CH_cards[CH_idx]]

            elif CH_cards[CH_idx] == 'NEXT_R':
                if square_pos_names[pos] == 'CH1':
                    pos = square_names_pos['R2']
                elif square_pos_names[pos] == 'CH2':
                    pos = square_names_pos['R3']
                else:
                    pos = square_names_pos['R1']

            elif CH_cards[CH_idx] == 'NEXT_U':
                if square_pos_names[pos] == 'CH1' or square_pos_names[pos] == 'CH3':
                    pos = square_names_pos['U1']
                else:
                    pos = square_names_pos['U2']

            elif CH_cards[CH_idx] == 'BACK':
                pos -= 3
                if pos < 0:
                    pos = 39

            CH_idx += 1
            if CH_idx == len(CH_cards):
                CH_idx = 0

        elif 'CC' in square_pos_names[pos]:
            if CC_cards[CC_idx] in square_names:
                pos = square_names_pos[CC_cards[CC_idx]]

            CC_idx += 1
            if CC_idx == len(CC_cards):
                CC_idx = 0

        square_counts[square_pos_names[pos]] += 1

    mod_str = str()
    for i in range(3):
        name = max(square_counts, key=square_counts.get)
        mod_str += str(square_names_pos[name])
        square_counts.pop(name)

    return mod_str


if __name__ == '__main__':
    print(compute())
