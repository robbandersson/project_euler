#
# Solution to Project Euler problem 42 - Coded triangle numbers
#
# https://gitlab.com/robbandersson/project_euler
#

import os
from src.functions import letter_to_num, is_triangle_num
data_dir = os.path.normpath('data')


def compute():
    word_list = list()
    with open(data_dir + '/p042_words.txt', 'r') as f:
        for line in f:
            current_line = line.split(",")
            for word in current_line:
                word_list.append(word.replace("\"", ""))

    num_triangle_words = 0
    for word in word_list:
        word_sum = 0
        for letter in word.lower():
            word_sum += letter_to_num(letter)
        if is_triangle_num(word_sum):
            num_triangle_words += 1

    return num_triangle_words


if __name__ == '__main__':
    print(compute())
