#
# Solution to Project Euler problem 108 - Diophantine Reciprocals I
#
# https://gitlab.com/robbandersson/project_euler
#
import os, sys
import math
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), "../src"))
from functions import prime_factorize, is_prime


def num_divisors(prime_factors):
    num = 1
    for prime in set(prime_factors):
        num *= (1 + prime_factors.count(prime))
    return num


def num_squared_divisors(prime_factors):
    num = 1
    for prime in set(prime_factors):
        num *= (1 + 2 * prime_factors.count(prime))
    return num


def get_next_prime(prime):
    if prime == 2:
        return 3
    else:
        p = prime + 2
        while not is_prime(p):
            p += 2
        return p


def get_connected_primes(prime):
    if prime == 2:
        return [2, 3]
    elif prime == 3:
        return [2, 3, 5]
    else:
        prime_list = []
        p = prime - 2
        while not is_prime(p):
            p -= 2
        prime_list.append(p)
        prime_list.append(prime)
        p = prime + 2
        while not is_prime(p):
            p += 2
        prime_list.append(p)
        return prime_list


def next_hcn(hcn):
    if hcn == 1:
        return 2
    elif hcn == 2:
        return 4
    elif hcn == 24:
        return 36
    
    p_factors = prime_factorize(hcn)
    p_max = get_connected_primes(max(p_factors))

    global candidates
    candidates = []

    for p in p_max:
        lb = int(math.log(p) / math.log(2))
        ub = 2 * int(math.log(get_next_prime(p)) / math.log(2))
        for k in range(lb, ub + 1):
            prod = 2 ** k
            candidate_hcn(prod, 2, k, hcn, p)

    for cand in sorted(candidates):
        if num_divisors(prime_factorize(cand)) > num_divisors(p_factors):
            return cand

def candidate_hcn(prod, prime, k, hcn, prime_max):
    next_prime = get_next_prime(prime)
    if k == 1:
        if next_prime <= prime_max:
            next_prod = prod * next_prime
            candidate_hcn(next_prod, next_prime, 1, hcn, prime_max)
        elif prod > hcn and prod <= 2 * hcn:
            candidates.append(prod)
    else:
        for i in range(1, k + 1):
            new_prod = prod * next_prime ** i
            if new_prod <= 2 * hcn:
                candidate_hcn(new_prod, next_prime, i, hcn, prime_max)                
    

def compute():
    num_hcns = 1
    hcn = 2  
    while num_divisors(prime_factorize(hcn)) < 2000:
        prev_hcn = hcn
        hcn = next_hcn(hcn)
    
    print(num_squared_divisors(prime_factorize(180180)))
    return hcn

    
if __name__ == '__main__':
    print(compute())
