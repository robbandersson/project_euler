# project_euler

The following project presents solutions to Project Euler problems (https://projecteuler.net/). The data/ folder contains .txt datasets for the solved problems. Most solutions are in Python, with some exceptions for Mathematica. 

101 - Lagrange polynomial
107 - Minumum spanning tree