import math
import numpy as np


def is_pal(x):
    """ Check if a number is a palindrome

    param x: number to check
    return:
    """
    if str(x) == str(x)[::-1]:
        return True


def is_prime(n):
    """ Check if a number is a prime

    :param n: number to check
    :return:
    """
    if n <= 1:
        return False
    if n <= 3:
        return True
    if (n % 2 == 0) or (n % 3 == 0):
        return False
    i = 5
    while i * i <= n:
        if (n % i == 0) or (n % (i + 2) == 0):
            return False
        i = i + 6
    return True


def prime_factorize(x):
    """ Factorize number into prime factors

    :param x:
    :return:
    """
    prime_list = []
    while True:
        if is_prime(x):
            prime_list.append(int(x))
            break
        for i in range(2, int(math.ceil(math.sqrt(x)))+1):
            if is_prime(i):
                if x % i == 0:
                    prime_list.append(i)
                    x = x / i
                    break
    return prime_list


def generate_primes(n):
    """ Generates all primes up to n

    :param n:
    :return:
    """
    primes = set()
    primes.add(2)
    i = 3
    while i < n:
        for p in primes:
            if i % p == 0:
                i += 2
                continue
        primes.add(i)
        i += 2
    return primes


def is_pandigital(num):
    """ Check if a number is pandigital

    :param num: number to check
    :return:
    """
    ref = ''
    for k in range(1, len(str(num)) + 1):
        ref += str(k)
    if sorted(str(num)) == sorted(ref):
        return True
    else:
        return False


def num_divisors(x):
    """ Find number of divisors of a number

    :param x:
    :return:
    """
    div_list = []
    for i in range(1, int(math.ceil(math.sqrt(x)))):
        if x % i == 0:
            div_list.append(i)
            div_list.append(x / i)
    return div_list


def letter_to_num(x):
    """ Returns the corresponding number of a letter, i.e., a -> 1, b -> 2, etc.

    :param x:
    :return:
    """
    return ord(x) - 96


def is_triangle_num(num):
    """ Checks if a number is in the sequence of triangle numbers

    :param num:
    :return:
    """
    triangle_num = 1
    n = 1
    while(triangle_num < num):
        n += 1
        triangle_num = n * (n + 1) / 2
    if triangle_num == num:
        return True


def is_square(n):
    """ Check if a number is square
    :param n:
    :return:
    """

    return int(np.sqrt(n) + 0.5) ** 2 == n


def euler_totient(n):
    """

    :param n:
    :return:
    """
    euler_t = n
    for p in set(prime_factorize(n)):
        euler_t *= (1 - 1 / p)
    return int(euler_t)


def is_coprime(a, b):
    """

    :param a:
    :param b:
    :return:
    """
    return math.gcd(a, b) == 1